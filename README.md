![](docs/images/logo.png)

# Candelapp

Candelapp is a multi-tenant platform that simplifies application development by providing a host of pre-built features such as user management, groups and roles, permission systems, API generation, static website generation, and a user-friendly interface. Offering a logical separation strategy, it allows a single instance to host many tenants, thereby enhancing scalability and maintainability.

### Why is it called Candelapp?

To pay tribute to the Candela, a noteworthy unit from the International System of Units.

### But... why ?

The primary objectives of Candelapp are centered around offering an environment that is both easy to use and highly flexible, enabling rapid and efficient project setup. The software is designed to streamline the process of data management, whether the application in question is simple or complex in nature.

Candelapp is engineered to be an effective tool for fast prototyping, allowing users to quickly sketch their data models and see them take shape in the form of fully functional APIs. Beyond just sketching, it also facilitates the creation of advanced applications with robust data management needs.

### Built with

- Django, django-rest-framework, django-tenant
- Quasar
- PostgreSQL


## Features

### Multi-tenant platform
Candelapp adopts a multi-tenancy approach, allowing one instance to host multiple tenants. This approach introduces a logical separation strategy, enabling you to efficiently manage resources, save costs, and maintain isolation among different tenants.

### API generation and documentation
It can automatically generate APIs based on the definition of models. Each model comprises multiple views, providing users with the necessary tools to manage its objects. This feature simplifies backend development and helps expedite the software delivery process. The API also provides the documentation describing all the endpoints.


### API sorting and filtering capabilities
Candelapp's API includes robust sorting and filtering functionalities for views. This allows users to easily sift through their data, sorting by any field in ascending or descending order. It also provides advanced filtering capabilities, letting users specify the exact subset of data they want to view, based on a range of criteria. This powerful feature enhances usability and data accessibility, allowing for more precise data analysis and efficient navigation.

### User management system
It includes a comprehensive user management system, allowing administrators to easily manage users, groups, and roles.

### Advanced permission system
With the permission system, Candelapp enables administrators to manage user, group, and object permissions per action (add, edit, view, delete) per model. This comprehensive system allows for granular access control, ensuring data security and compliance.

### Multilingual support
Candelapp also boasts an international feature that caters to a global user base. This functionality allows users to add as many languages as needed to their applications. With this multilingual support, users can create objects specific to each language, ensuring content is accessible and relevant to users around the world.

### User Activity Logging
A log system designed to monitor user activity is available. It records interactions with models and model items, tracks user management actions, and logs user connections, but also records the states of objects both before and after an action is performed. This activity log provides a detailed audit trail of user actions, allowing administrators to review actions taken in the system over time. This system respects the GDPR guidelines.

### Static website generation
Candelapp can generate static websites using Jinja templates, enabling quick website deployment with a minimal footprint. This feature allows users to create lightweight, high-performance websites that are easy to maintain and scale.

### User interface
Candelapp is equipped with a user-friendly interface that streamlines the management of its numerous features. Moreover, this interface offers white-labeling capabilities. Users can customize the interface with their own branding elements, such as colors and logos, to reflect the identity of their specific project.

### i18n / internationalization support
The platform is fully translated into both English and French.

### Screenshots

###### Create a model view
![](docs/images/modelview.png)

###### Create an object view
![](docs/images/createobject.png)

###### Edit a permission view
![](docs/images/permissionview.png)


## Limitations. What it does not do (yet ?)
1. Migrations: Editing a model has no effect on old objects. Only the new ones will benefit from the changes. This means that you either have to accept that not all objects will necessarily reflect the model, or you have to manually migrate the objects as required.

2. Pagination: Candelapp's API does not yet support pagination. As of now, when you make a request, the API returns all results in one batch instead of dividing them into smaller, manageable pages. This may pose a challenge when dealing with large volumes of data.

3. Static generation with relationships: Currently, Candelapp does not support static generation with relationships, including one-to-many and many-to-one relationships. This means that when generating static websites using Jinja templates, related data cannot be automatically populated or interconnected. As a result, creating pages that require complex data relationships might need additional manual work.

## Known bugs/issues
1. Setting multiple relationship type fields, such as many-to-one or one-to-many, requires a large number of requests to retrieve all the data. This process needs to be reworked.

2. Some forms either fail to display errors when something goes wrong or do not provide enough detail to be help.

3. If no SMTP server is set, a user will be unable to set up or change their password.

## Future developments
Candelapp will evolve and improve over time. Here are the planned future developments for the platform:

1. Resolution of current limitations: Efforts will be made to address the current limitations. Future versions of Candelapp will support pagination in the API, allowing users to manage large data sets more easily. Moreover, static generation with relationships, including one-to-many and many-to-one relationships, will be incorporated to simplify the creation of interconnected data pages.

2. Response size optimization: To enhance performance and limit payload size, the default response will be shortened to only return the name and primary keys from objects. Full responses will still be available, but will need to be specifically requested via a query parameter.

3. Performance enhancement: Query optimization is in the pipeline to improve overall system performance. This would mean faster retrieval of data and a smoother, more efficient user experience.

4. New field types: The platform is set to introduce new field types such as DateField, LocationField, and MultipleLocationField. This will offer users more options to customize their models and better represent their data.

5. Field validation rules: More specific validation rules are planned to be implemented for different field types. For instance, this would include min/max character validations for TextField, and email validation to ensure data consistency and accuracy in the case of this field type.

6. Field constraints: Candelapp will add the capability to define constraints between fields, for instance, making field1 required if field2 is not filled. This adds an extra layer of data integrity and customization.

7. Improved static generation process: Improvements are also planned for the static generation process. Users will be able to use live data to generate locally, making templating easier and more efficient.

8. Quasar migration: Candelapp uses Quasar v1 and needs to be updated to v2 (vue3)

9. UI Enhancement: Refine the error handling process. The upcoming changes aim to provide clearer, more informative error messages and handle exceptions more gracefully, ultimately improving the user experience and facilitating troubleshooting.

10. Add a profile view to the user interface that allows for easy retrieval of the bearer token, updating of information, and changing of passwords.

## Documentation

- [How does it work](docs/howdoesitwork.md)
- [Static website generation](docs/static.md)
- [Permissions](docs/permissions.md)
- [Multilingual support](docs/multilingual.md)
- [Contributing](CONTRIBUTING.md)
- TO DO: Deploy

## License

This project is licensed under the terms of the MIT license. See the [LICENSE](LICENSE.md) file.
