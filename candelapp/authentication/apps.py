from django.apps import AppConfig


class AuthConfig(AppConfig):
    name = "candelapp.authentication"

    def ready(self):
        """Connect signals."""
        from . import signals  # NOQA
