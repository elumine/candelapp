from django.conf import settings
from django.core.mail import send_mail
from django.utils.translation import gettext as _

from candelapp.authentication.utils import TokenManager
from candelapp.settings.models import Settings


def send_confirmation_email(user):
    if not settings.EMAIL_HOST and settings.EMAIL_FROM:
        return

    candelapp_settings = Settings.objects.first()

    token = TokenManager.generate_token(user)
    send_mail(
        subject=_(f"Activation code for your {candelapp_settings.site_name} account"),
        message=_(
            "Hello,\n\n"
            "Thank you for creating your account. We are delighted to have you "
            "join the %(site_name)s project. In order to finalize your "
            "registration, please return to the connexion page to enter "
            "the following activation code "
            "(valid for %(validity_duration)s minutes):\n\n"
            "%(token)s\n\n"
            "This will allow you to set your login password and log in.\n\n"
            "See you soon! The %(site_name)s team."
        )
        % {
            "site_name": candelapp_settings.site_name,
            "validity_duration": int(settings.PASSWORD_RESET_TIMEOUT / 60),
            "token": token,
        },
        from_email=settings.EMAIL_FROM,
        recipient_list=[user.email],
    )


def send_reset_password_email(user):
    if not settings.EMAIL_HOST and settings.EMAIL_FROM:
        return
    token = TokenManager.generate_token(user)
    candelapp_settings = Settings.objects.first()
    send_mail(
        subject=_(f"Forgotten password on {candelapp_settings.site_name}"),
        message=_(
            "Hello,\n\n"
            "Need to reset your password?\n\n"
            "Return to the connexion page and use the following activation code "
            "(valid for %(validity_duration)s minutes):\n\n"
            "%(token)s\n\n"
            "If you have not requested a password reset, please ignore this email.\n\n"
            "See you soon! The %(site_name)s team."
        )
        % {
            "validity_duration": int(settings.PASSWORD_RESET_TIMEOUT / 60),
            "token": token,
            "site_name": candelapp_settings.site_name,
        },
        from_email=settings.EMAIL_FROM,
        recipient_list=[user.email],
    )
