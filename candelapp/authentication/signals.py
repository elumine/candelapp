from typing import Type

from django.db.models.signals import post_save
from django.dispatch import receiver

from django.contrib.auth import get_user_model

from candelapp.authentication.mails import send_confirmation_email


User = get_user_model()


@receiver(post_save, sender=User)
def user_add(sender: Type[User], instance: Type[User], created: bool, **kwargs) -> None:
    """When a user is created, if smtp settings are set, send a confirmation mail."""
    if created and not instance.email_confirmed:
        send_confirmation_email(user=instance)
