from django.core import mail
from django.test import override_settings

from candelapp.authentication.factories import AdminFactory, UserFactory
from candelapp.settings.models import Settings
from candelapp.tools import test_utils


@override_settings(EMAIL_HOST="mail.candelapp.com", EMAIL_FROM="hello@candelapp.com")
class EmailTestCase(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    """Authentication tests."""

    def test_email_settings(self):
        # With a Hello site_name
        settings = Settings.objects.first()
        settings.site_name = "Hello"
        settings.save(update_fields=["site_name"])
        # When creating a user that should have a not confirmed email
        with override_settings(EMAIL_HOST=""):
            user = UserFactory(email_confirmed=False)
            assert user.email_confirmed is False
            # It should not send an email as email settings are not set
            assert len(mail.outbox) == 0

        # But when they are
        user = UserFactory(email_confirmed=False)
        assert user.email_confirmed is False
        # A confirmation email has been sent
        assert len(mail.outbox) == 1
        assert mail.outbox[0].subject == "Activation code for your Hello account"

    def test_user_creation(self):
        # When creating a user and an admin user
        non_admin_user = UserFactory(email_confirmed=False)
        admin_user = AdminFactory(email_confirmed=False)

        # We should have 2 mails sent
        assert len(mail.outbox) == 2

        # When saving again
        for user in [non_admin_user, admin_user]:
            user.first_name = "Giscard"
            user.save(update_fields=["last_name"])
            # No email should be sent and the mailbox should still have only 2
            assert len(mail.outbox) == 2
