"""Authentication serializers tests."""
from datetime import timedelta

from django.conf import settings

from django.contrib.auth.models import Group

from rest_framework.exceptions import ErrorDetail
from freezegun import freeze_time

from candelapp.authentication import serializers
from candelapp.authentication.factories import AdminFactory, UserFactory
from candelapp.authentication.utils import TokenManager
from candelapp.tools import test_utils


class UserSerializerTest(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    """Test UserSerializer."""

    @classmethod
    def setUpTestData(cls):
        """Set up users."""
        super().setUpTestData()
        cls.admin_user = AdminFactory()
        cls.user = UserFactory()

    def test_user_serializer(self):
        """Test serializer with an admin and a simple user."""
        serializer = serializers.UserSerializer(self.admin_user)
        assert serializer.data == {
            "pk": self.admin_user.pk,
            "first_name": self.admin_user.first_name,
            "last_name": self.admin_user.last_name,
            "email": self.admin_user.email,
            "groups": [],
            "extras": {},
            "is_confirmed": True,
            "is_admin": True,
        }

        serializer = serializers.UserSerializer(self.user)
        assert serializer.data == {
            "pk": self.user.pk,
            "first_name": self.user.first_name,
            "last_name": self.user.last_name,
            "email": self.user.email,
            "groups": [],
            "extras": {},
            "is_confirmed": True,
            "is_admin": False,
        }

    def test_read_only_fields(self):
        """Test updating read only fields from a user"""
        for field in [
            "pk",
            "is_admin",
            "is_confirmed",
        ]:
            serializer = serializers.UserSerializer(
                self.user,
                data={field: 42 if field == "pk" else True},
                partial=True,
            )
            assert serializer.is_valid() is True
            instance = serializer.save()
            # Field did not update
            assert getattr(instance, field) != (42 or True)

    def test_update_groupes(self):
        """Test updating groups from a user."""
        group1 = Group.objects.create(name="g1")
        group2 = Group.objects.create(name="g2")
        serializer = serializers.UserSerializer(
            self.admin_user,
            data={"groups": [group1.pk, group2.pk]},
            partial=True,
        )
        assert serializer.is_valid() is True
        user = serializer.save()
        assert list(user.groups.all()) == [group1, group2]

        serializer = serializers.UserSerializer(
            self.admin_user,
            data={"groups": [group1.pk]},
            partial=True,
        )
        assert serializer.is_valid() is True
        user = serializer.save()
        assert list(user.groups.all()) == [group1]

    def test_update_with_non_available_email(self):
        serializer = serializers.UserSerializer(
            self.admin_user,
            data={"email": self.admin_user.email},
            partial=True,
        )
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "email": [ErrorDetail(string="This email is not available.", code="invalid")]
        }


class PasswordChangeSerializerTest(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    def test_passwordchange_serializer_errors(self):
        # With incomplete data
        data = {"hey": "ho"}
        serializer = serializers.PasswordChangeSerializer(data=data)
        assert serializer.is_valid() is False
        # We should have errors on what fields we are missing
        assert serializer.errors == {
            "new_password": [ErrorDetail(string="This field is required.", code="required")],
            "re_new_password": [ErrorDetail(string="This field is required.", code="required")],
        }

        # With too weak passwords
        data = {"new_password": "hello", "re_new_password": "hello"}
        serializer = serializers.PasswordChangeSerializer(data=data)
        assert serializer.is_valid() is False
        # We should have errors on how weak are the passwords
        assert serializer.errors == {
            "new_password": [
                ErrorDetail(
                    string="This password is too short. It must contain at least 8 characters.",
                    code="invalid",
                ),
                ErrorDetail(string="This password is too common.", code="invalid"),
            ]
        }

        # With re type password mismatching
        data = {"new_password": "hellohowlow@", "re_new_password": "hellohowlow"}
        serializer = serializers.PasswordChangeSerializer(data=data)
        assert serializer.is_valid() is False
        # We should have errors that the passwords are mismatching
        assert serializer.errors == {
            "non_field_errors": [ErrorDetail(string="Password mismatch.", code="password_mismatch")]
        }

    def test_passwordchange_serializer(self):
        # With a nice payload
        data = {"new_password": "hellohowlow@", "re_new_password": "hellohowlow@"}
        serializer = serializers.PasswordChangeSerializer(data=data)
        # It should be valid
        assert serializer.is_valid() is True


class EmailValidationSerializerTest(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    @classmethod
    def setUpTestData(cls):
        """Set up users."""
        super().setUpTestData()
        cls.admin_user = AdminFactory()
        cls.user = UserFactory()

    def test_emailvalidation_serializer_errors(self):
        # With incomplete data
        data = {"hey": "ho"}
        serializer = serializers.EmailValidationSerializer(data=data)
        assert serializer.is_valid() is False
        # We should have errors on what fields we are missing
        assert serializer.errors == {
            "new_password": [ErrorDetail(string="This field is required.", code="required")],
            "re_new_password": [ErrorDetail(string="This field is required.", code="required")],
            "token": [ErrorDetail(string="This field is required.", code="required")],
        }

        # With a wrong token
        data = {"new_password": "hellohowlow@", "re_new_password": "hellohowlow@", "token": "hello"}
        serializer = serializers.EmailValidationSerializer(data=data)
        assert serializer.is_valid() is False
        # We should have errors, token is not valid
        assert serializer.errors == {
            "token": [
                ErrorDetail(
                    string="The token is not valid or has expired.",
                    code="invalid",
                )
            ]
        }

        # With an expired token
        from datetime import datetime

        token = TokenManager.generate_token(self.user)
        time = datetime.now() + timedelta(seconds=settings.PASSWORD_RESET_TIMEOUT + 1)
        data = {"new_password": "hellohowlow@", "re_new_password": "hellohowlow@", "token": token}
        serializer = serializers.EmailValidationSerializer(data=data)

        # By jumping in time
        with freeze_time(time):
            # It should not be valid anymore
            assert serializer.is_valid() is False
            assert serializer.errors == {
                "token": [
                    ErrorDetail(
                        string="The token is not valid or has expired.",
                        code="invalid",
                    )
                ]
            }

    def test_emailvalidation_serializer(self):
        # By creating a new token for a non confirmed user
        user = UserFactory(email_confirmed=False)
        token = TokenManager.generate_token(user)
        data = {"new_password": "hellohowlow@", "re_new_password": "hellohowlow@", "token": token}
        serializer = serializers.EmailValidationSerializer(data=data)

        # It should be valid
        assert serializer.is_valid() is True
