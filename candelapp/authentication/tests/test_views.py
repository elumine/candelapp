from django.core import mail
from django.test import override_settings

from django.contrib.auth import get_user_model

from rest_framework.exceptions import ErrorDetail

from candelapp.authentication.factories import AdminFactory, UserFactory
from candelapp.authentication.utils import TokenManager
from candelapp.groups.factories import GroupFactory
from candelapp.history.models import HistoryLog
from candelapp.settings.models import Settings
from candelapp.tools import test_utils

User = get_user_model()


ENDPOINTS = {
    "users": "/api/v1/auth/users/",
    "userme": "/api/v1/auth/me/",
    "toggle_admin": "/api/v1/auth/users/{}/toggle_admin/",
    "signup": "/api/v1/auth/signup/",
    "password_set_token": "/api/v1/auth/password_set_token/",
    "password_change": "/api/v1/auth/password_change/",
    "password_reset_token": "/api/v1/auth/password_reset_token/",
}


class UsersViewsetTestCase(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    """Users tests"""

    @classmethod
    def setUpTestData(cls):
        """Set up users."""
        super().setUpTestData()
        cls.admin_user = AdminFactory()
        cls.non_admin_user = UserFactory()

    def test_get(self):
        """Test to get users."""
        # Not logged in
        self._test_get(ENDPOINTS["users"], None, 401)

        # Non admin user
        self._test_get(ENDPOINTS["users"], self.non_admin_user, 403)

        # Test list
        response = self._test_get(ENDPOINTS["users"], self.admin_user, 200)
        assert len(response.json()) == 2

        # Test get user
        response = self._test_get(
            f"{ENDPOINTS['users']}{str(self.non_admin_user.pk)}/",
            self.admin_user,
            200,
        )
        assert response.json()["email"] == self.non_admin_user.email

    def test_get_filter(self):
        # By filtering users with is_admin qp
        response = self._test_get(
            f"{ENDPOINTS['users']}?is_admin=false",
            self.admin_user,
            200,
        )
        assert len(response.json()) == 1
        response = self._test_get(
            f"{ENDPOINTS['users']}?is_admin=FaLsE",
            self.admin_user,
            200,
        )
        assert len(response.json()) == 1

        # By filter users with groups
        # With no user with groups
        response = self._test_get(
            f"{ENDPOINTS['users']}?with_groups=true",
            self.admin_user,
            200,
        )
        assert len(response.json()) == 0

        # With user with groups
        group = GroupFactory()
        self.non_admin_user.groups.add(group)
        self.non_admin_user.save()
        response = self._test_get(
            f"{ENDPOINTS['users']}?with_groups=true",
            self.admin_user,
            200,
        )
        assert len(response.json()) == 1

        # By filtering confirmed users
        # With confirmed users
        response = self._test_get(
            f"{ENDPOINTS['users']}?is_confirmed=true",
            self.admin_user,
            200,
        )
        assert len(response.json()) == 2

        # With only one user confirmed
        self.non_admin_user.email_confirmed = False
        self.non_admin_user.save()
        response = self._test_get(
            f"{ENDPOINTS['users']}?is_confirmed=true",
            self.admin_user,
            200,
        )
        assert len(response.json()) == 1

    def test_patch_user(self):
        data = {"last_name": "Cox"}
        # Not logged in
        response = self._test_patch(
            f"{ENDPOINTS['users']}{str(self.non_admin_user.pk)}/",
            None,
            401,
            data,
        )

        # Non admin user
        response = self._test_patch(
            f"{ENDPOINTS['users']}{str(self.non_admin_user.pk)}/",
            self.non_admin_user,
            403,
            data,
        )

        # Admin user
        response = self._test_patch(
            f"{ENDPOINTS['users']}{str(self.non_admin_user.pk)}/",
            self.admin_user,
            200,
            data,
        )
        assert response.json()["last_name"] == "Cox"

        assert HistoryLog.objects.count() == 1
        hl = HistoryLog.objects.first()
        assert (
            hl.translated_action
            == f"User {self.non_admin_user.email} has been modified by {self.admin_user.email}."
        )
        assert hl.type == "user_change"
        assert hl.prev_state["last_name"] != "Cox"
        assert hl.new_state["last_name"] == "Cox"

    def test_delete_user(self):
        another_user = UserFactory()

        # Not logged in
        self._test_delete(
            f"{ENDPOINTS['users']}{str(self.non_admin_user.pk)}/",
            None,
            401,
        )

        # Non admin user
        self._test_delete(
            f"{ENDPOINTS['users']}{str(another_user.pk)}/",
            self.non_admin_user,
            403,
        )

        # Admin user
        self._test_delete(
            f"{ENDPOINTS['users']}{str(another_user.pk)}/",
            self.admin_user,
            204,
        )

        assert HistoryLog.objects.count() == 1
        hl = HistoryLog.objects.first()
        assert (
            hl.translated_action
            == f"User {another_user.email} has been deleted by {self.admin_user.email}."
        )
        assert hl.type == "user_delete"

    def test_toggle_admin(self):
        """Test to set and unset admin role to user"""
        # As user
        self._test_get(
            ENDPOINTS["toggle_admin"].format(str(self.non_admin_user.pk)),
            self.non_admin_user,
            403,
        )

        # Set admin role to user
        self._test_get(
            ENDPOINTS["toggle_admin"].format(str(self.non_admin_user.pk)),
            self.admin_user,
            200,
        )
        self.non_admin_user.refresh_from_db()
        assert self.non_admin_user.is_staff is True
        assert self.non_admin_user.is_superuser is True
        assert self.non_admin_user.is_admin is True

        assert HistoryLog.objects.count() == 1
        hl = HistoryLog.objects.first()
        assert hl.translated_action == (
            f"User {self.non_admin_user.email} has been promoted "
            f"admin by {self.admin_user.email}."
        )
        assert hl.type == "user_toggle_admin"
        assert hl.prev_state["is_superuser"] is False
        assert hl.new_state["is_superuser"] is True

        # Unset admin role to user
        self._test_get(
            ENDPOINTS["toggle_admin"].format(str(self.non_admin_user.pk)),
            self.admin_user,
            200,
        )
        self.non_admin_user.refresh_from_db()
        assert self.non_admin_user.is_staff is False
        assert self.non_admin_user.is_superuser is False
        assert self.non_admin_user.is_admin is False

        assert HistoryLog.objects.count() == 2
        hl = HistoryLog.objects.last()
        assert hl.translated_action == (
            f"User {self.non_admin_user.email} has been demoted "
            f"from admin by {self.admin_user.email}."
        )
        assert hl.type == "user_toggle_admin"
        assert hl.prev_state["is_superuser"] is True
        assert hl.new_state["is_superuser"] is False


class UserMeViewsetTestCase(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    """User me tests"""

    @classmethod
    def setUpTestData(cls):
        """Set up users."""
        super().setUpTestData()
        cls.admin_user = AdminFactory()
        cls.non_admin_user = UserFactory()

    def test_get_self(self):
        """Test to get self."""
        # No logged in
        self._test_get(ENDPOINTS["userme"], None, 401)

        # Non admin user
        response = self._test_get(ENDPOINTS["userme"], self.non_admin_user, 200)
        assert response.json()["email"] == self.non_admin_user.email

        # admin user
        response = self._test_get(ENDPOINTS["userme"], self.admin_user, 200)
        assert response.json()["email"] == self.admin_user.email

    def test_patch_self(self):
        """Test to patch self."""
        data = {"last_name": "Cox"}
        # No logged in
        self._test_patch(ENDPOINTS["userme"], None, 401)

        # Non admin user
        response = self._test_patch(ENDPOINTS["userme"], self.non_admin_user, 200, data)
        assert response.json()["last_name"] == "Cox"

        assert HistoryLog.objects.count() == 1
        hl = HistoryLog.objects.first()
        assert (
            hl.translated_action
            == f"User {self.non_admin_user.email} has been modified by {self.non_admin_user.email}."
        )
        assert hl.type == "user_change"

        # admin user
        response = self._test_patch(ENDPOINTS["userme"], self.admin_user, 200, data)
        assert response.json()["last_name"] == "Cox"

        assert HistoryLog.objects.count() == 2
        hl = HistoryLog.objects.last()
        assert (
            hl.translated_action
            == f"User {self.admin_user.email} has been modified by {self.admin_user.email}."
        )
        assert hl.type == "user_change"

    def test_post_self(self):
        """Test to post self."""
        # No logged in
        self._test_post(ENDPOINTS["userme"], None, 401)

        # Non admin user
        self._test_post(ENDPOINTS["userme"], self.non_admin_user, 405)

        # admin user
        self._test_post(ENDPOINTS["userme"], self.admin_user, 405)

    def test_delete_self(self):
        """Test to delete self."""
        # No logged in
        self._test_patch(ENDPOINTS["userme"], None, 401)

        # Non admin user
        self._test_delete(ENDPOINTS["userme"], self.non_admin_user, 405)

        # admin user
        self._test_delete(ENDPOINTS["userme"], self.admin_user, 405)


@override_settings(EMAIL_HOST="mail.candelapp.com", EMAIL_FROM="hello@candelapp.com")
class SignupViewTestCase(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    @classmethod
    def setUpTestData(cls):
        """Set up users."""
        super().setUpTestData()
        cls.admin_user = AdminFactory()
        cls.non_admin_user = UserFactory()
        cls.settings = Settings.objects.first()
        cls.settings.site_name = "Hello"
        cls.settings.save(update_fields=["site_name"])

    def test_get(self):
        """Test to get signup endpoint."""
        # Method get not authorized
        self._test_get(ENDPOINTS["signup"], None, 405)

    def test_post(self):
        """Test to post signup endpoint."""
        # Anonymous can post but no data
        self._test_post(ENDPOINTS["signup"], None, 400)

        # Same with authenticated user
        self._test_post(ENDPOINTS["signup"], self.non_admin_user, 400)

    def test_signup(self):
        """Test to post signup endpoint."""
        # A user needs at least a first name, a last name and an email
        response = self._test_post(ENDPOINTS["signup"], None, 400, {"email": "hello@how.low"})
        assert response.data == {
            "first_name": [ErrorDetail(string="This field is required.", code="required")],
            "last_name": [ErrorDetail(string="This field is required.", code="required")],
        }

        # An authenticated user can but with no data
        self._test_post(
            ENDPOINTS["signup"],
            None,
            204,
            {"email": "hello@how.low", "first_name": "Hello", "last_name": "How Low"},
        )
        user = User.objects.get(email="hello@how.low")
        assert user.email_confirmed is False
        assert user.is_admin is False

        assert len(mail.outbox) == 1
        assert mail.outbox[0].subject == "Activation code for your Hello account"

        assert HistoryLog.objects.count() == 1
        hl = HistoryLog.objects.first()
        assert hl.translated_action == f"User {user.email} has been created by an anonymous user."
        assert hl.type == "user_add"

        # When trying to create a user with an already used email
        response = self._test_post(
            ENDPOINTS["signup"],
            None,
            400,
            {"email": "hello@how.low", "first_name": "Hello", "last_name": "How Low"},
        )
        # It should raise a 400 with the correct error
        assert response.data == {
            "email": [
                ErrorDetail(string="user with this email address already exists.", code="unique")
            ]
        }


class PasswordSetTokenTestCase(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    @classmethod
    def setUpTestData(cls):
        """Set up users."""
        super().setUpTestData()
        cls.admin_user = AdminFactory()
        cls.non_admin_user = UserFactory()

    def test_get(self):
        """Test to get password_set_token endpoint."""
        # Method get not authorized
        self._test_get(ENDPOINTS["password_set_token"], None, 405)

    def test_post(self):
        """Test to post password_set_token endpoint."""
        # Anonymous authorized but no data
        self._test_post(ENDPOINTS["password_set_token"], None, 400)

        # Same with an authenticated user
        self._test_post(ENDPOINTS["password_set_token"], self.non_admin_user, 400)

    def test_verify_user(self):
        # With a confirmed user
        token = TokenManager.generate_token(self.non_admin_user)
        data = {"new_password": "hellohowlow@", "re_new_password": "hellohowlow@", "token": token}

        response = self._test_post(ENDPOINTS["password_set_token"], self.non_admin_user, 400, data)
        assert response.data == {
            "token": [ErrorDetail(string="The token is not valid or has expired.", code="invalid")]
        }

        # With an unconfirmed user
        user = UserFactory(email_confirmed=False, password=None)
        token = TokenManager.generate_token(user)
        data = {"new_password": "hellohowlow@", "re_new_password": "hellohowlow@", "token": token}
        # It should return a 204 and the user should be confirmed with a password
        response = self._test_post(ENDPOINTS["password_set_token"], None, 204, data)
        user.refresh_from_db()
        assert user.email_confirmed is True
        assert bool(user.password) is True


class PasswordChangeViewTestCase(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    @classmethod
    def setUpTestData(cls):
        """Set up users."""
        super().setUpTestData()
        cls.admin_user = AdminFactory()
        cls.non_admin_user = UserFactory()

    def test_get(self):
        """Test to get password_change endpoint."""
        # Method get not authorized
        self._test_get(ENDPOINTS["password_change"], self.non_admin_user, 405)

    def test_post(self):
        """Test to post password_change endpoint."""
        # Anonymous authorized should not be able to post
        self._test_post(ENDPOINTS["password_change"], None, 401)

        # An authenticated user can but with no data
        self._test_post(ENDPOINTS["password_change"], self.non_admin_user, 400)

    def test_change_password(self):
        admin_password = self.admin_user.password
        user_password = self.non_admin_user.password

        for user in [self.admin_user, self.non_admin_user]:
            data = {"new_password": "hellohowlow@", "re_new_password": "hellohowlow@"}
            self._test_post(ENDPOINTS["password_change"], user, 204, data)

        self.admin_user.refresh_from_db()
        self.non_admin_user.refresh_from_db()

        assert self.admin_user.password != admin_password
        assert self.non_admin_user.password != user_password


class PasswordResetTokenViewTestCase(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    @classmethod
    def setUpTestData(cls):
        """Set up users."""
        super().setUpTestData()
        cls.admin_user = AdminFactory()
        cls.non_admin_user = UserFactory()
        cls.settings = Settings.objects.first()
        cls.settings.site_name = "Hello"
        cls.settings.save(update_fields=["site_name"])

    def test_get(self):
        """Test to get password_reset_token endpoint."""
        # Method get not authorized
        self._test_get(ENDPOINTS["password_reset_token"], None, 405)

    def test_post(self):
        """Test to post password_reset_token endpoint."""
        # Anonymous can post but no data
        self._test_post(ENDPOINTS["password_reset_token"], None, 400)

        # Same with authenticated user
        self._test_post(ENDPOINTS["password_reset_token"], self.non_admin_user, 400)

    def test_reset_token(self):
        """Test to post password_reset_token endpoint."""
        # Unknown email. Will return 204 anyway but wont send token by email
        self._test_post(ENDPOINTS["password_reset_token"], None, 204, {"email": "hello@how.low"})
        assert len(mail.outbox) == 0

        # An authenticated user can but with no data
        self._test_post(
            ENDPOINTS["password_reset_token"], None, 204, {"email": self.non_admin_user.email}
        )
        assert len(mail.outbox) == 1
        assert mail.outbox[0].subject == "Forgotten password on Hello"
