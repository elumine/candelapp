"""Authentication related tests."""

from datetime import timedelta

from django.conf import settings
from django.utils import timezone

from django.contrib.auth import get_user_model

from freezegun import freeze_time
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from candelapp.authentication.factories import AdminFactory, UserFactory
from candelapp.history.models import HistoryLog
from candelapp.tools import test_utils


ENDPOINTS = {
    "login": "/api/v1/auth/login/",
    "me": "/api/v1/auth/me/",
}


User = get_user_model()


class AuthenticationTestCase(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    """Authentication tests."""

    @classmethod
    def setUpTestData(cls):
        """Set up users."""
        super().setUpTestData()
        cls.admin_user = AdminFactory(first_name="Bob", last_name="Kelso")
        cls.non_admin_user = UserFactory(first_name="John", last_name="Dorian")

    def test_users(self):
        """Test creation of an admin with a factory."""
        assert User.objects.count() == 2

        assert self.admin_user.first_name == "Bob"
        assert self.admin_user.last_name == "Kelso"
        assert self.admin_user.is_superuser is True

        assert self.non_admin_user.first_name == "John"
        assert self.non_admin_user.last_name == "Dorian"
        assert self.non_admin_user.is_superuser is False

    def test_authenticate_users(self):
        """Test authentication of users."""
        # Login is a POST endpoint
        self._test_get(ENDPOINTS["login"], None, 405)

        # Unknown user
        data = {"email": "reid@sacredheart.com", "password": "c4nd3l4pp"}
        response = self._test_post(ENDPOINTS["login"], None, 400, data, as_json=True)
        assert response.json() == {
            "non_field_errors": ["Unable to log in with provided credentials."]
        }
        # Non admin users
        data = {"email": self.non_admin_user.email, "password": "c4nd314pp"}
        response = self._test_post(ENDPOINTS["login"], None, 200, data, as_json=True)

        assert HistoryLog.objects.count() == 1
        hl = HistoryLog.objects.first()
        assert hl.translated_action == (
            f"User {self.non_admin_user.email} has logged in with the ip address 127.0.x.x."
        )
        assert hl.type == "user_connect"

        # Admin user
        data = {"email": self.admin_user.email, "password": "c4nd314pp"}
        self._test_post(ENDPOINTS["login"], None, 200, data, as_json=True)

        assert HistoryLog.objects.count() == 2
        hl = HistoryLog.objects.last()
        assert hl.translated_action == (
            f"User {self.admin_user.email} has logged in with the ip address 127.0.x.x."
        )
        assert hl.type == "user_connect"

    def test_authenticated_request(self):
        """Test to get with an authenticated request then logout."""
        data = {"email": self.admin_user.email, "password": "c4nd314pp"}
        response = self._test_post(ENDPOINTS["login"], None, 200, data, as_json=True)
        token = response.json()["token"]

        client = APIClient(HTTP_HOST=settings.HTTP_HOST)
        client.credentials(HTTP_AUTHORIZATION="Bearer " + token)
        response = client.get(ENDPOINTS["me"])
        assert response.status_code == 200

    def test_token_expiration(self):
        """Test that a token expires after API_TOKEN_MAX_AGE."""
        data = {"email": self.admin_user.email, "password": "c4nd314pp"}
        response = self._test_post(ENDPOINTS["login"], None, 200, data, as_json=True)
        token = response.json()["token"]

        # At this point a token is created when logged in
        assert Token.objects.filter(key=token).exists() is True

        client = APIClient(HTTP_HOST=settings.HTTP_HOST)
        client.credentials(HTTP_AUTHORIZATION="Bearer " + token)
        response = client.get(ENDPOINTS["me"])
        assert response.status_code == 200

        # Here, the token is deleted by the permissions because of expiration
        time = timezone.now() + timedelta(seconds=settings.API_TOKEN_MAX_AGE + 1)
        with freeze_time(time):
            response = client.get(ENDPOINTS["me"])
            assert response.status_code == 401
            assert Token.objects.filter(key=token).exists() is False

    def test_wipe_token_when_switching_roles(self):
        # Create a new user and an authentication token
        user = UserFactory()
        token = Token.objects.create(user=user).key

        # Check he can query
        client = APIClient(HTTP_HOST=settings.HTTP_HOST)
        client.credentials(HTTP_AUTHORIZATION="Bearer " + token)
        response = client.get(ENDPOINTS["me"])
        assert response.status_code == 200

        # Swich its role to admin and ensure the token has been deleted
        user.toggle_admin()
        user.refresh_from_db()
        assert user.is_admin is True
        with self.assertRaises(User.auth_token.RelatedObjectDoesNotExist):
            user.auth_token
        response = client.get(ENDPOINTS["me"])
        assert response.status_code == 401

        # Recreate it again
        token = Token.objects.create(user=user).key
        client = APIClient(HTTP_HOST=settings.HTTP_HOST)
        client.credentials(HTTP_AUTHORIZATION="Bearer " + token)
        response = client.get(ENDPOINTS["me"])
        assert response.status_code == 200

        # Switch again to user role
        user.toggle_admin()
        user.refresh_from_db()
        assert user.is_admin is False
        with self.assertRaises(User.auth_token.RelatedObjectDoesNotExist):
            user.auth_token
        response = client.get(ENDPOINTS["me"])
        assert response.status_code == 401
