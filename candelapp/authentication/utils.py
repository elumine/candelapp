from base64 import urlsafe_b64decode, urlsafe_b64encode
from binascii import Error as B64Error

from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import default_token_generator


User = get_user_model()


class TokenManager:
    """
    Token manager that generates and checks tokens for users validation.

    It uses the django default_token_generator. The check validates against
    the PASSWORD_RESET_TIMEOUT setting.
    """

    user = None

    @staticmethod
    def generate_token(user) -> str:
        token = default_token_generator.make_token(user)
        token_with_email = f"{token}:{user.email}"
        return urlsafe_b64encode(token_with_email.encode()).decode()

    def check_token(self, b64_token: str) -> bool:
        token = None
        email = None
        try:
            token_with_email = urlsafe_b64decode(b64_token.encode())
            token, email = token_with_email.decode().split(":")
        except (B64Error, ValueError):
            return False

        try:
            self.user = User.objects.get(email=email)
        except User.DoesNotExist:
            return False

        return default_token_generator.check_token(self.user, token)

    @property
    def get_user_from_token(self):
        if self.user is None:
            raise RuntimeError(
                "You have to call the check_token method first in order to get the user."
            )
        return self.user
