from copy import deepcopy

from django.db.models import Q
from django.utils.translation import gettext_lazy as _

from rest_framework.exceptions import ParseError
from rest_framework.filters import BaseFilterBackend

from candelapp.drf_url_filters import constants


class URLFilterBackend(BaseFilterBackend):
    """
    A simple backend to filter querysets from query params.

    Ex: /?one_field=1&another_field=hello will be transformed as such:
    WHERE one_field LIKE 1 or another_field LIKE hello

    Multiple values on the same field has to be separated by a comma or dashes.
    Comma separated values will be considered will use the OR operand.
    Dash will be considered as BETWEEN.

    Ex: /?field=1,2 will be transformed as such:

    WHERE field LIKE 1 OR field like 2

    Range values are separated by dashes.

    Ex: /?field__range=1-2 will be transformed as such:
    WHERE field BETWEEN 1 AND 2

    Chaining is possible.

    Ex: /?field=1-2 will&another_field=hello,bye be transformed as such:
    WHERE field BETWEEN 1 and 2
    AND another_field LIKE hello or another_field LIKE bye
    """

    queryset = None

    def empty_queryset(self):
        return self.queryset.none()

    def filter_with_query_params(self, filters):
        return self.queryset.filter(filters)

    def filter_queryset(self, request, queryset, view):
        """
        Return a filtered queryset.
        """
        self.queryset = queryset

        query_params = deepcopy(request.query_params)
        # Remove ordering query params if it exists
        # The ordering backend manages this part already
        if query_params.get("ordering"):
            query_params.pop("ordering")

        # Get the fields list from the first object
        try:
            self.model_fields = queryset.first().model.fields
        except AttributeError:
            return self.empty_queryset()

        filters = self.process_params(query_params)
        return self.filter_with_query_params(filters)

    def process_params(self, params):
        """
        Iter over the query params for checkups and add them as Q filters.
        Ensure model fields used exist and lookups are available.
        """
        complex_filters = Q()
        for key, values in params.items():
            field_and_lookup = key.split("__")
            field = None
            lookup_expr = None

            # If no lookup, lets assume it's exact
            if len(field_and_lookup) == 1:
                field = field_and_lookup[0]
                lookup_expr = "exact"
            elif len(field_and_lookup) == 2:
                field = field_and_lookup[0]
                lookup_expr = field_and_lookup[1]
            else:
                raise ParseError(_("Filtering relations is not yet available."))

            model_field = self.get_model_field(field)
            field_type = model_field.get("type")
            field_name = model_field.get("internal_name")
            self.check_lookup(lookup_expr, field_name, field_type)

            # Created and modified fields are outside the data field
            # but all the fields in data should use the lookup relation
            if field not in ["created", "modified"]:
                field_and_lookup = f"data__{key}"
            else:
                field_type = "date"

            match field_type:
                case "number":
                    q_filters = self.build_number_filters(
                        lookup_expr, field, values, field_and_lookup
                    )
                case "date":
                    raise NotImplementedError
                case "manytoone":
                    q_filters = self.build_fk_filters(lookup_expr, field, values, field_and_lookup)
                case _:
                    q_filters = self.build_default_filters(
                        lookup_expr, field, values, field_and_lookup
                    )

            complex_filters &= q_filters

        return complex_filters

    def get_model_field(self, field):
        for model_field in self.model_fields:
            if field == model_field["internal_name"]:
                return model_field
        raise ParseError(_(f"{field} is not a valid field to filter."))

    def check_lookup(self, lookup, field_name, field_type):
        lookups = getattr(constants, f"{field_type.upper()}_LOOKUPS")
        if lookup not in lookups:
            raise ParseError(
                _("{} is not a valid lookup for the field {}. Available lookups for {}: {}").format(
                    lookup, field_name, field_name, lookups
                )
            )

    def build_default_filters(self, lookup_expr, field, values, field_and_lookup):
        """Default filter builder."""
        q_filters = Q()
        # Use OR on all the comma separated values
        for value in values.split(","):
            q_filters |= Q(**{field_and_lookup: value})
        return q_filters

    def build_fk_filters(self, lookup_expr, field, values, field_and_lookup):
        """Manytoone filter builder.

        Use Django ORM's __contains lookup functionality to effectively implement
        these filters based on list values.

        Ex:
        ?manytoone=1
        is transformed to data__manytoone__contains=[1]
        """
        q_filters = Q()
        # Use OR on all the comma separated values
        for value in values.split(","):
            q_filters |= Q(**{f"{field_and_lookup}__contains": [int(value)]})
        return q_filters

    def build_number_filters(self, lookup_expr, field, values, field_and_lookup):
        """
        Number filter builder.

        Ensure query params are converted to float to filter numbers with the ORM.
        When the param is a range, convert it to a list of two floats.
        """
        q_filters = Q()

        range_values = values.split("-")
        if lookup_expr == "range":
            try:
                range_values[0] = float(range_values[0])
                range_values[1] = float(range_values[1])
            except (KeyError, TypeError):
                raise ParseError(_("Range filtering requires two values separated by a dash."))
            q_filters = Q(**{field_and_lookup: [range_values[0], range_values[1]]})
        else:
            # Use OR on all the comma separated values
            for value in values.split(","):
                try:
                    value = float(value)
                except ValueError:
                    raise ParseError(
                        _(f"{field} with {lookup_expr} lookup " "expected a number. Got a string.")
                    )
                # If model_field type is a number, convert the values as floats
                # So the orm can filter as numbers and not strings
                q_filters |= Q(**{field_and_lookup: float(value)})
        return q_filters
