NUMBER_LOOKUPS = [
    "exact",
    "gt",
    "gte",
    "lt",
    "lte",
    "range",
]

TEXT_LOOKUPS = [
    "contains",
    "icontains",
    "iexact",
    "exact",
    "endswith",
    "iendswith",
    "startswith",
    "istartwidth",
]

MULTIPLECHOICE_LOOKUPS = [
    "contains",
    "icontains",
]

CHOICE_LOOKUPS = [
    "contains",
    "icontains",
    "exact",
    "iexact",
]

MANYTOONE_LOOKUPS = [
    "exact",
]

ONETOMANY_LOOKUPS = [
    "exact",
]

DATE_LOOKUPS = []

DATETIME_LOOKUPS = []
