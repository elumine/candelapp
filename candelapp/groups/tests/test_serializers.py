from django.contrib.auth.models import Group

from candelapp.authentication.factories import AdminFactory, UserFactory
from candelapp.groups.factories import GroupFactory
from candelapp.groups import serializers
from candelapp.tools import test_utils


class GroupSerializerTest(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    @classmethod
    def setUpTestData(cls):
        """Set up users."""
        super().setUpTestData()
        cls.admin_user = AdminFactory()
        cls.user = UserFactory()
        cls.group = GroupFactory()

    def test_group_serializer(self):
        # When serializing a group
        serializer = serializers.GroupSerializer(self.group)
        # It should return data with pk name and user_set fields
        assert serializer.data == {"pk": self.group.pk, "name": self.group.name, "user_set": []}

        # When adding a user in the group
        self.user.groups.add(self.group)
        serializer = serializers.GroupSerializer(self.group)

        # The user_set should contain the user pk
        assert serializer.data["user_set"] == [self.user.pk]

    def test_read_only_fields(self):
        # When patching read only fields
        for field in [
            "pk",
            "user_set",
        ]:
            serializer = serializers.GroupSerializer(
                self.group,
                data={field: 42 if field == "pk" else [2, 3]},
                partial=True,
            )
            assert serializer.is_valid() is True
            instance = serializer.save()
            # Fields should not be updated
            assert getattr(instance, field) != (42 or [2, 3])

    def test_create_group(self):
        # When populating a serializer with a name
        serializer = serializers.GroupSerializer(data={"name": "Hello group"})

        # The instance should be named "Hello group"
        serializer.is_valid()
        instance = serializer.save()
        assert instance.name == "Hello group"
        assert Group.objects.filter(name="Hello group").count() == 1
