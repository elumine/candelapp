from candelapp.authentication.factories import AdminFactory, UserFactory
from candelapp.groups.factories import GroupFactory
from candelapp.tools import test_utils


ENDPOINTS = {
    "groups": "/api/v1/groups/",
}


class GroupsViewsetTestCase(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    """Users tests"""

    @classmethod
    def setUpTestData(cls):
        """Set up users."""
        super().setUpTestData()
        cls.admin_user = AdminFactory()
        cls.non_admin_user = UserFactory()
        cls.group = GroupFactory()

    def test_get(self):
        """Test to get groups."""
        # Not logged in
        self._test_get(ENDPOINTS["groups"], None, 401)

        # Non admin user
        self._test_get(ENDPOINTS["groups"], self.non_admin_user, 403)

        # Test list
        response = self._test_get(ENDPOINTS["groups"], self.admin_user, 200)
        assert len(response.json()) == 1

        # Test get user
        response = self._test_get(
            f"{ENDPOINTS['groups']}{str(self.group.pk)}/",
            self.admin_user,
            200,
        )
        assert response.json()["name"] == self.group.name

    def test_patch(self):
        data = {"name": "Hyperion"}
        # Not logged in
        response = self._test_patch(
            f"{ENDPOINTS['groups']}{str(self.group.pk)}/",
            None,
            401,
            data,
        )

        # Non admin user
        response = self._test_patch(
            f"{ENDPOINTS['groups']}{str(self.group.pk)}/",
            self.non_admin_user,
            403,
            data,
        )

        # Admin user
        response = self._test_patch(
            f"{ENDPOINTS['groups']}{str(self.group.pk)}/",
            self.admin_user,
            200,
            data,
        )
        assert response.json()["name"] == "Hyperion"

    def test_delete(self):
        another_group = GroupFactory()

        # Not logged in
        self._test_delete(
            f"{ENDPOINTS['groups']}{str(another_group.pk)}/",
            None,
            401,
        )

        # Non admin user
        self._test_delete(
            f"{ENDPOINTS['groups']}{str(another_group.pk)}/",
            self.non_admin_user,
            403,
        )

        # Admin user
        self._test_delete(
            f"{ENDPOINTS['groups']}{str(another_group.pk)}/",
            self.admin_user,
            204,
        )
