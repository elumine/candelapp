from django.urls import path

from rest_framework import routers

from candelapp.history import views

router = routers.SimpleRouter()

urlpatterns = router.urls

urlpatterns += [
    path("", views.HistoryLogListView.as_view(), name="logs_list"),
]
