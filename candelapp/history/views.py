from rest_framework import filters
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAdminUser

from candelapp.history.serializers import HistoryLogModelSerializer
from candelapp.history.models import HistoryLog


class HistoryLogListView(ListAPIView):
    """
    History log list view.

    You can filter the queryset by searching users email or logs type.

    Ex:

    ?search=user_add

    ?search=user@candelapp.com
    """

    queryset = HistoryLog.objects.order_by("-timestamp")
    serializer_class = HistoryLogModelSerializer
    permission_classes = [IsAdminUser]
    filter_backends = [filters.SearchFilter]
    search_fields = ["type", "user__email"]
