from django.utils import translation


class CustomLocaleMiddleware:
    """
    Get language from requets and apply it.

    TODO: Add a preferred_language to the user model
    This will have a priority over the language from request.
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        user_language = translation.get_language_from_request(request, check_path=True)

        # auth = request.META.get("HTTP_AUTHORIZATION")
        # if auth and auth.startswith("Bearer "):
        #     token_key = auth[7:]  # remove 'Bearer ' prefix
        #     try:
        #         token = Token.objects.get(key=token_key)
        #         user = token.user
        #     except Token.DoesNotExist:
        #         pass

        translation.activate(user_language)
        request.LANGUAGE_CODE = translation.get_language()

        response = self.get_response(request)
        response["Vary"] = "Accept-Language"

        return response
