"""International related seiralizers."""

from rest_framework import serializers

from candelapp.international import models


class LanguageSerializer(serializers.ModelSerializer):
    """Serializer for Languages."""

    name = serializers.SerializerMethodField()

    class Meta:
        model = models.Language
        fields = ["pk", "name", "code"]
        read_only_fields = ["pk"]

    def get_name(self, value):
        return value.get_translated_name()
