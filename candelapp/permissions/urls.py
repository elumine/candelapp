from rest_framework import routers

from candelapp.permissions import views


router = routers.SimpleRouter()

router.register(r"", views.PermissionsViewSet)

urlpatterns = []

urlpatterns += router.urls
