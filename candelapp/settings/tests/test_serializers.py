"""Settings serializer tests."""
from rest_framework.exceptions import ErrorDetail

from candelapp.international.models import Language
from candelapp.settings import models
from candelapp.settings.serializers import (
    InterfaceSettingsSerializer,
    LanguageSettingsSerializer,
    SettingsSerializer,
    PasswordFTPSerializer,
)

from candelapp.tools import test_utils
from candelapp.tools.encryption import PasswordManager


class SettingsSerializerTest(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    """Test SettingsSerializer."""

    @classmethod
    def setup_class(cls):
        cls.settings = models.Settings.objects.get()

    def test_serializer_with_languages(self):
        """Test serializer with languages."""
        # Set english as primary then french and spanish as secondary
        serializer = SettingsSerializer(
            self.settings,
            data={"primary_language": 38, "secondary_languages": [48, 40]},
            partial=True,
        )
        assert serializer.is_valid() is True
        serializer.save()

        # Try to patch english as secondary language while being primary
        # with full payload
        serializer = SettingsSerializer(
            self.settings,
            data={"primary_language": 38, "secondary_languages": [48, 38]},
            partial=True,
        )
        assert serializer.is_valid() is False

        # Try to patch english as secondary language without primary_language data
        serializer = SettingsSerializer(
            self.settings, data={"secondary_languages": [48, 38]}, partial=True
        )
        assert serializer.is_valid() is False

        # Try to patch french as primary language without secondary_language data
        serializer = SettingsSerializer(
            self.settings,
            data={
                "primary_language": 48,
            },
            partial=True,
        )
        assert serializer.is_valid() is False

    def test_serializer(self):
        # No data
        serializer = SettingsSerializer(self.settings, data={}, partial=True)
        assert serializer.is_valid() is True

        # All data without password

        fr_lang = Language.objects.get(code="fr")

        serializer = SettingsSerializer(
            self.settings,
            data={
                "ftp_host": "localhost.host",
                "ftp_port": 22,
                "ftp_user": "user",
                "primary_language": 48,
                "secondary_languages": [],
                "site_name": "Fs0c13ty",
                "logo": "https://logo.png",
                "colors": {"primary_color": "#fff"},
            },
            partial=True,
        )
        assert serializer.is_valid() is True
        serializer.save()
        assert serializer.validated_data == {
            "ftp_host": "localhost.host",
            "ftp_port": 22,
            "ftp_user": "user",
            "primary_language": fr_lang,
            "secondary_languages": [],
            "site_name": "Fs0c13ty",
            "logo": "https://logo.png",
            "colors": {"primary_color": "#fff"},
        }


class InterfaceSettingsSerializerTest(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    """Test InterfaceSettingsSerializer."""

    def test_serializer(self):
        settings = models.Settings.objects.first()
        settings.logo = "http://logo.png"
        settings.colors = {"primary_color": "#fff"}
        settings.site_name = "Fsociety"
        settings.save(update_fields=["logo", "colors", "site_name"])

        # Interface settings serializer only serializer interface related fields
        serializer = InterfaceSettingsSerializer(settings)
        assert serializer.data == {
            "colors": {"primary_color": "#fff"},
            "logo": "http://logo.png",
        }

        # Serializer is read only
        serializer = InterfaceSettingsSerializer(
            settings,
            data={
                "ftp_host": "localhost.host",
                "ftp_port": 22,
                "ftp_user": "user",
            },
            partial=True,
        )
        serializer.is_valid()
        serializer.save()
        assert serializer.data == {
            "colors": {"primary_color": "#fff"},
            "logo": "http://logo.png",
        }


class LanguageSerializerTest(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    """Test LanguageSerializer."""

    @classmethod
    def setup_class(cls):
        cls.settings = models.Settings.objects.get()

    def test_serializer_with_languages(self):
        """Test serializer with languages."""
        # Default
        serializer = LanguageSettingsSerializer(self.settings)
        assert serializer.data == {
            "primary_language": {
                "pk": self.settings.primary_language.pk,
                "code": "en",
                "name": "English",
            },
            "secondary_languages": [],
        }

        # What en as primary language and es,fr as secondary ones should return
        serializer = SettingsSerializer(
            self.settings,
            data={"primary_language": 38, "secondary_languages": [40, 48]},
            partial=True,
        )
        serializer.is_valid()
        serializer.save()

        self.settings.refresh_from_db()

        serializer = LanguageSettingsSerializer(self.settings)
        assert serializer.data["primary_language"] == {
            "pk": self.settings.primary_language.pk,
            "code": "en",
            "name": "English",
        }
        assert {
            "pk": self.settings.secondary_languages.get(code="es").pk,
            "code": "es",
            "name": "Spanish",
        } in serializer.data["secondary_languages"]
        assert {
            "pk": self.settings.secondary_languages.get(code="fr").pk,
            "code": "fr",
            "name": "French",
        } in serializer.data["secondary_languages"]


class PasswordSerializerTest(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    """Test LanguageSerializer."""

    @classmethod
    def setup_class(cls):
        cls.settings = models.Settings.objects.get()

    def test_serializer(self):
        # Password is write only
        serializer = PasswordFTPSerializer(self.settings)
        assert serializer.data == {}

    def test_patch_password(self):
        # By patching with a password with less than 8 characters
        serializer = PasswordFTPSerializer(
            self.settings, data={"ftp_password": "hello"}, partial=True
        )
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "ftp_password": [
                ErrorDetail(
                    string="Ensure this field has at least 8 characters.", code="min_length"
                )
            ]
        }

        # By patching with a correct password
        serializer = PasswordFTPSerializer(
            self.settings, data={"ftp_password": "password"}, partial=True
        )
        assert serializer.is_valid() is True
        instance = serializer.save()

        # The decrypted password should be the same from the data
        pm = PasswordManager()
        decrypted_password = pm.decrypt(instance.ftp_password)
        assert decrypted_password == "password"
