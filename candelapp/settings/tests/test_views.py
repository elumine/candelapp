"""Settings related tests."""
from candelapp.authentication.factories import AdminFactory, UserFactory

from candelapp.settings.models import Settings
from candelapp.tools import test_utils


ENDPOINTS = {
    "interface": "/api/v1/settings/interface/",
    "languages": "/api/v1/settings/languages/",
    "settings": "/api/v1/settings/settings/",
    "update_ftp_password": "/api/v1/settings/update_ftp_password/",
}


class SettingsViewTestCase(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    """Settings tests"""

    @classmethod
    def setUpTestData(cls):
        """Set up users."""
        super().setUpTestData()
        cls.admin_user = AdminFactory()
        cls.non_admin_user = UserFactory()

    def test_get(self):
        """Test to get settings."""
        # No logged in
        self._test_get(ENDPOINTS["settings"], None, 401)

        # Non admin user
        self._test_get(ENDPOINTS["settings"], self.non_admin_user, 403)

        response = self._test_get(ENDPOINTS["settings"], self.admin_user, 200)
        assert response.json() == {
            "ftp_host": "",
            "ftp_user": "",
            "ftp_port": 21,
            "primary_language": 38,
            "secondary_languages": [],
            "site_name": "",
            "site_url": "",
            "colors": {},
            "logo": "",
        }

    def test_post(self):
        """Test to post settings."""
        # Settings are not meant to be created
        self._test_post(ENDPOINTS["settings"], self.admin_user, 405)

    def test_patch(self):
        """Test to patch settings."""
        # Patch ftp host
        data = {
            "ftp_host": "localhost",
        }

        response = self._test_patch(ENDPOINTS["settings"], self.admin_user, 200, data, as_json=True)
        assert response.json() == {
            "ftp_host": "localhost",
            "ftp_port": 21,
            "ftp_user": "",
            "primary_language": 38,
            "secondary_languages": [],
            "site_name": "",
            "site_url": "",
            "colors": {},
            "logo": "",
        }

        # Patch all fields except password and languages
        data = {
            "ftp_host": "localhost.host",
            "ftp_user": "user",
            "ftp_port": 22,
            "site_name": "Fs0c13ty",
            "site_url": "https://text.example",
        }
        response = self._test_patch(ENDPOINTS["settings"], self.admin_user, 200, data, as_json=True)
        assert response.json() == {
            "ftp_host": "localhost.host",
            "ftp_port": 22,
            "ftp_user": "user",
            "primary_language": 38,
            "secondary_languages": [],
            "site_name": "Fs0c13ty",
            "site_url": "https://text.example",
            "colors": {},
            "logo": "",
        }

        # Patch languages
        data = {
            "primary_language": 48,  # French
            "secondary_languages": [38, 40],  # English, Spanish
        }
        response = self._test_patch(
            ENDPOINTS["settings"],
            self.admin_user,
            200,
            data,
            as_json=True,
        )
        assert response.json() == {
            "ftp_host": "localhost.host",
            "ftp_port": 22,
            "ftp_user": "user",
            "primary_language": 48,
            "secondary_languages": [38, 40],
            "site_name": "Fs0c13ty",
            "site_url": "https://text.example",
            "colors": {},
            "logo": "",
        }

    def test_patch_password(self):
        password = Settings.objects.first().ftp_password
        data = {"ftp_password": "passw0rd1"}

        # When updating the password with the password as a user
        # It should return a 403
        self._test_patch(
            ENDPOINTS["update_ftp_password"],
            self.non_admin_user,
            403,
            data,
            compare=False,
            as_json=True,
        )

        # When updating the password with an admin
        # The password should be updated
        self._test_patch(
            ENDPOINTS["update_ftp_password"],
            self.admin_user,
            200,
            data,
            compare=False,
            as_json=True,
        )
        assert password != Settings.objects.first().ftp_password


class InterfaceSettingsViewTestCase(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    """Interface settings tests"""

    def test_get(self):
        """Test to get interface settings."""
        # Anonymous
        response = self._test_get(ENDPOINTS["interface"], None, 200)
        assert response.json() == {"colors": {}, "logo": ""}

    def test_patch(self):
        """Test to patch interface settings."""
        # Patch ftp host as anonymous
        data = {
            "ftp_host": "localhost",
        }

        self._test_patch(ENDPOINTS["interface"], None, 401, data, as_json=True)

        # As admin user
        admin_user = UserFactory(
            email="alderson@allsafe.com",
            first_name="Eliott",
            last_name="Alderson",
        )
        self._test_patch(ENDPOINTS["interface"], admin_user, 405, data, as_json=True)


class LanguageSettingsViewTestCase(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    """Language settings tests"""

    @classmethod
    def setUpTestData(cls):
        """Set up users."""
        super().setUpTestData()
        cls.admin_user = AdminFactory()
        cls.non_admin_user = UserFactory()

    def test_get(self):
        """Test to get settings."""
        # No logged in
        self._test_get(ENDPOINTS["languages"], None, 401)

        # Non admin user
        self._test_get(ENDPOINTS["languages"], self.non_admin_user, 403)

        response = self._test_get(ENDPOINTS["languages"], self.admin_user, 200)
        primary_lang = Settings.objects.first().primary_language
        assert response.json() == {
            "primary_language": {
                "pk": primary_lang.pk,
                "code": "en",
                "name": "English",
            },
            "secondary_languages": [],
        }

    def test_post(self):
        """Test to post languages."""
        # Settings are not meant to be created
        self._test_post(ENDPOINTS["settings"], self.admin_user, 405)
