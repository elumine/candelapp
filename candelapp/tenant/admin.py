from candelapp.tenant import models
from django.contrib import admin
from django_tenants.admin import TenantAdminMixin


class SuperAdminRestrictedMixin:
    """
    Mixin to restrict modules and views.

    It allows only superusers on the public schema to add or
    edit tenants.
    """

    def is_tenant_restricted(self, request):
        return all([request.tenant.schema_name == "public", request.user.is_superuser])

    def has_view_permission(self, request, obj=None):
        return self.is_tenant_restricted(request)

    def has_add_permission(self, request, obj=None):
        return self.is_tenant_restricted(request)

    def has_change_permission(self, request, obj=None):
        return self.is_tenant_restricted(request)

    def has_delete_permission(self, request, obj=None):
        return self.is_tenant_restricted(request)

    def has_module_permission(self, request, obj=None):
        return self.is_tenant_restricted(request)


@admin.register(models.Tenant)
class TenantAdmin(SuperAdminRestrictedMixin, TenantAdminMixin, admin.ModelAdmin):
    """Tenant admin."""

    search_fields = ("name",)
    list_display = ("name",)


@admin.register(models.Domain)
class DomainAdmin(SuperAdminRestrictedMixin, TenantAdminMixin, admin.ModelAdmin):
    """Domain admin."""

    search_fields = ("domain",)
    list_display = ("domain", "tenant")
