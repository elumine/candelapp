from django.db import models

from django.utils.translation import gettext_lazy as _

from django_tenants.models import TenantMixin, DomainMixin


class Tenant(TenantMixin):
    """Tenant model."""

    name = models.CharField(max_length=100)
    created_on = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.name


class Domain(DomainMixin):
    """Domain model."""

    class Meta:
        verbose_name = _("Domaines des client")

    def __str__(self):
        return self.domain
