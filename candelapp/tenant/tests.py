"""Tenant related tests."""
from django.test import override_settings

from django.contrib.auth import get_user_model

from django_tenants.utils import schema_context

from candelapp.authentication.factories import AdminFactory
from candelapp.tools import test_utils


ENDPOINTS = {
    "login": "/api/v1/auth/login/",
    "me": "/api/v1/auth/me/",
}

User = get_user_model()


@override_settings(HTTP_HOST="localhost")
class PublicSchemaTestCase(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    """Public schema tests."""

    def test_public_urls(self):
        """Test that there are no reachable urls with public schema."""
        with self.assertRaises(LookupError):
            self._test_post(ENDPOINTS["login"], None, 404)
        with self.assertRaises(LookupError):
            self._test_get(ENDPOINTS["me"], None, 404)


class TenantschemaTestCase(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    """Non-public schemas tests."""

    @classmethod
    def setUpTestData(cls):
        """Set up users."""
        super().setUpTestData()
        with schema_context("fsociety"):
            AdminFactory(email="alderson@allsafe.com")

        with schema_context("sacredheart"):
            AdminFactory(email="reid@sacredheart.com")
            AdminFactory(email="dorian@sacredheart.com")

    def test_tenant_urls(self):
        """Test that there are reachable urls with fsociety schema."""
        data = {"email": "s4ms3p101@test.com", "password": "hey"}
        self._test_post(ENDPOINTS["login"], None, 400, data)
        self._test_get(ENDPOINTS["me"], None, 401)

    def test_tenants_objects(self):
        """Test objects in public, fsociety and sacredheart schema."""
        with schema_context("fsociety"):
            assert User.objects.count() == 1
            users_list = list(User.objects.values_list("email", flat=True))

            assert "alderson@allsafe.com" in users_list
            assert "reid@sacredheart.com" not in users_list

        with schema_context("sacredheart"):
            assert User.objects.count() == 2
            users_list = list(User.objects.values_list("email", flat=True))
            assert "dorian@sacredheart.com" in users_list
            assert "reid@sacredheart.com" in users_list
            assert "alderson@allsafe.com" not in users_list

    def test_authentication(self):
        """Test authentication through fsociety default schemas."""
        # User from sacredheart schemas should not be able to authenticate
        # with a user from fsociety one
        data = {"email": "reid@sacredheart.com", "password": "c4nd3l4pp"}
        response = self._test_post(ENDPOINTS["login"], None, 400, data, as_json=True)
        assert response.json() == {
            "non_field_errors": ["Unable to log in with provided credentials."]
        }

        # But a user from the same schema will
        data = {"email": "alderson@allsafe.com", "password": "c4nd314pp"}
        self._test_post(ENDPOINTS["login"], None, 200, data, as_json=True)
