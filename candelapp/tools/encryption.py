import base64

from django.conf import settings

from cryptography.fernet import Fernet
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC


class PasswordManager:
    """
    Simple symmetric password encryption manager for storing passwords.

    Uses secret key as salt.

    https://cryptography.io/en/latest/fernet/#using-passwords-with-fernet
    """

    def __init__(self, **kwargs):
        salt = settings.SECRET_KEY.encode()
        password = base64.b16encode(salt)
        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            length=32,
            salt=salt,
            iterations=390000,
        )
        key = base64.urlsafe_b64encode(kdf.derive(password))
        self.fernet = Fernet(key)

    def encrypt(self, value: str) -> str:
        encrypted_value = self.fernet.encrypt(value.encode())
        return encrypted_value.decode()

    def decrypt(self, encrypted_value: str) -> str:
        byte_value = self.fernet.decrypt(encrypted_value.encode())
        return byte_value.decode()
