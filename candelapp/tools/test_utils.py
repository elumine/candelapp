"""Tests related tools."""
import collections
from datetime import datetime
import io
import json
import uuid

from django.http import QueryDict
from django.conf import settings
from django.utils.dateparse import parse_datetime

from rest_framework.response import Response
from rest_framework.test import APIClient, APIRequestFactory, APITestCase

from candelapp.drf_url_filters.backend import URLFilterBackend


def get_gif(name="image.gif"):
    """Return gif."""
    gif = io.BytesIO(
        b"GIF87a\x01\x00\x01\x00\x80\x01\x00\x00\x00\x00ccc,\x00"
        b"\x00\x00\x00\x01\x00\x01\x00\x00\x02\x02D\x01\x00;"
    )
    gif.name = name
    return gif


def get_png(name="image.png"):
    """Return png."""
    png = io.BytesIO(
        b"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x01\x00\x00\x00\x01\x08"
        b"\x02\x00\x00\x00\x90wS\xde\x00\x00\x00\x0cIDATx\x9cc```\x00\x00\x00\x04"
        b"\x00\x01\xf6\x178U\x00\x00\x00\x00IEND\xaeB`\x82"
    )
    png.name = name
    return png


def get_pdf():
    """Return pdf."""
    pdf = io.BytesIO(b"\x25\x50\x44\x46\x2d\x31\x2e\x34\x0a\x31\x20;")
    pdf.name = "doc.pdf"
    return pdf


initial_fields = [
    {"verbose_name": "Title", "required": True, "type": "text"},
    {"verbose_name": "Description", "type": "text", "required": False},
]


class APITestCaseWithData(APITestCase):
    """Custom API test case with data."""

    pass


def check_error_message(response: Response, field, error_text):
    """Test that response contains error_text for field error."""
    content = response.json()
    assert field in content, f"'{field}' not in {content}"
    if isinstance(content[field], str):
        assert error_text in content[field], f"'{error_text}' not in {content}"
    if isinstance(content[field], list):
        is_present = False
        for error in content[field]:
            if error_text in error:
                is_present = True
        assert is_present is True, f"'{error_text}' not in {content}"


class TestAPIMixin:
    """Generic tests for endpoints."""

    def create_session(self, user):
        self.client = APIClient(HTTP_HOST=settings.HTTP_HOST)
        if user:
            self.client.force_login(user)

    def _compare(self, value, expected, key):
        # Fixes drf renderering of nested serializer
        if isinstance(expected, collections.OrderedDict):
            expected = dict(expected)

        if isinstance(expected, datetime) and isinstance(value, str):
            value = parse_datetime(value)
            assert expected == value, "Expected '{}' to be '{}'".format(key, expected)
        elif isinstance(expected, uuid.UUID) or isinstance(expected, float):
            assert value == str(expected)
        else:
            assert value == expected, "Expected '{}' to be '{}'".format(key, expected)
            assert type(value) == type(expected)

    def _test_get(self, endpoint, user, status_code, expected_data=None):
        """
        Test get method.

        Test retrieve on the endpoint with the user.
        If status_code is 200, test the content of the response
        it should match self.
        """
        self.create_session(user)

        response = self.client.get(endpoint)

        assert response.status_code == status_code

        if status_code != 200:
            return response

        if expected_data is not None:
            content = response.json()

            assert set(content.keys()) == set(expected_data.keys())
            for key, value in expected_data.items():
                self._compare(content[key], value, key)
        return response

    def _test_patch(
        self,
        endpoint,
        user,
        status_code,
        patch_data=None,
        read_only_fields=None,
        as_json=False,
        compare=True,
    ):
        """Test patch method."""
        if not read_only_fields:
            read_only_fields = []

        self.create_session(user)

        if as_json is True:
            response = self.client.patch(
                endpoint, data=json.dumps(patch_data), content_type="application/json"
            )
        else:
            response = self.client.patch(endpoint, data=patch_data)

        assert response.status_code == status_code, response.data

        # Exit now if not 200 expected
        if status_code != 200:
            return response

        content = response.json()
        # Edit expected_data to match get_data and patch_data
        # except read_only_fields
        if compare:
            patched = {
                key: value for (key, value) in patch_data.items() if key not in read_only_fields
            }

            for key, value in patched.items():
                self._compare(content[key], value, key)
        return response

    def _test_post(
        self,
        endpoint,
        user,
        status_code,
        post_data=None,
        read_only_fields=None,
        as_json=False,
    ):
        """Test post method."""
        if not read_only_fields:
            read_only_fields = []

        self.create_session(user)

        if as_json is True:
            response = self.client.post(
                endpoint, data=json.dumps(post_data), content_type="application/json"
            )
        else:
            response = self.client.post(endpoint, data=post_data)

        assert response.status_code == status_code, getattr(response, "data", None)

        if status_code not in [200, 201]:
            return response

        if type(response) != "application/json":
            return response

        content = response.json()

        patched = {key: value for (key, value) in post_data.items() if key not in read_only_fields}
        for key, value in patched.items():
            self._compare(content[key], value, key)
        return response

    def _test_delete(self, endpoint, user, status_code, post_data=None):
        """Test delete method."""
        self.create_session(user)

        response = self.client.delete(endpoint, data=post_data)
        assert response.status_code == status_code
        return response


class TestQueryParamsMixin:
    """
    Factory mixin to create request with params.

    Returns a queryset.
    """

    @classmethod
    def setUpTestData(cls):
        cls.backend = URLFilterBackend()
        cls.rf = APIRequestFactory()

    def setUp(self):
        self.request = self.rf.get("candelapp.com")

    def _test_qp(self, params, model):
        modelitem_qs = model.cmodelitem_set.all()
        self.request.query_params = QueryDict(params)
        qs = self.backend.filter_queryset(self.request, modelitem_qs, None)
        return qs
