from django.utils.translation import gettext_lazy as _


STATUSES = (
    ("active", _("Active")),
    ("archived", _("Archived")),
    ("draft", _("Draft")),
    ("inactive", _("Inactive")),
    ("pending", _("Pending")),
)

DICT_STATUSES = dict(STATUSES)


MODEL_TYPES = (
    ("simple", _("Simple object model")),
    ("multiple", _("Multiple objects model")),
)


DATA_FIELD_TYPES = (
    "choice",
    "color",
    "file",
    "manytoone",
    "onetomany",
    "image",
    "multiplechoice",
    "number",
    "text",
    "wysiwyg",
)

# These names cannot be used as model fields
RESERVED_FIELD_NAMES = (
    "created",
    "data",
    "modified",
    "name",
    "type",
    "verbose_name",
    "url",
    "category_id",
    "id",
    "parent_id",
    "lang_id",
)
