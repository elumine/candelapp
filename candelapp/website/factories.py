"""Website related factories."""
import factory
import faker

from django.core.files.base import ContentFile
from django.utils.text import slugify


from candelapp.settings.models import Settings
from candelapp.website import models


fake = faker.Faker()


class CImageFactory(factory.django.DjangoModelFactory):
    """Factory for CImage."""

    class Meta:
        model = models.CImage

    image = factory.LazyAttribute(
        lambda _: ContentFile(
            factory.django.ImageField()._make_data({"width": 1024, "height": 768}),
            "example.webp",
        )
    )


class CFileFactory(factory.django.DjangoModelFactory):
    """Factory for CImage."""

    class Meta:
        model = models.CFile

    file = factory.django.FileField(filename="invoice.pdf", file__data=b"pdf")


class CModelCategoryFactory(factory.django.DjangoModelFactory):
    """Factory for a CModelCategory."""

    class Meta:
        model = models.CModelCategory

    name = factory.Sequence(lambda n: "category_{}".format(n))


class CModelFactory:
    def __new__(cls, name=None, verbose_name=None, type=None, category=None, fields=None):
        obj = models.CModel()
        obj.verbose_name = verbose_name or fake.name()
        obj.type = type or "simple"
        obj.category = category or CModelCategoryFactory()
        obj.fields = fields or [
            {
                "internal_name": "title",
                "type": "text",
                "verbose_name": "Title",
                "required": True,
            },
            {
                "internal_name": "description",
                "type": "text",
                "verbose_name": "Description",
                "required": False,
            },
            {
                "internal_name": "written",
                "type": "text",
                "verbose_name": "Written",
                "required": False,
                "extras": [
                    {"key": "key1", "value": "value"},
                    {"key": "key2", "value": "value2"},
                ],
            },
            {
                "internal_name": "picture",
                "type": "image",
                "verbose_name": "Picture",
                "required": False,
            },
            {
                "internal_name": "invoice",
                "type": "file",
                "verbose_name": "Invoice",
                "required": False,
                "extras": [
                    {"key": "key1", "value": "value"},
                    {"key": "key2", "value": "value2"},
                ],
            },
            {
                "internal_name": "price",
                "type": "number",
                "verbose_name": "Price",
                "required": False,
                "extras": [{}],
            },
            {
                "internal_name": "content",
                "type": "wysiwyg",
                "verbose_name": "Content",
                "required": False,
                "extras": [{}],
            },
            {
                "internal_name": "fruits",
                "verbose_name": "Fruits",
                "type": "choice",
                "choices": ["apple", "orange"],
                "required": False,
                "extras": [{}],
            },
            {
                "internal_name": "animals",
                "verbose_name": "Animals",
                "type": "multiplechoice",
                "choices": ["cats", "dogs", "ferrets"],
                "required": False,
                "extras": [{}],
            },
        ]
        obj.save()
        return obj


class CModelItemFactory:
    def __new__(cls, model=None, groups=[], parent=None, lang=None, data=None):
        obj = models.CModelItem()
        obj.model = model or CModelFactory()
        obj.parent = parent
        obj.lang = lang or Settings.objects.first().primary_language
        obj.data = data or {
            "title": fake.word(),
            "description": fake.catch_phrase(),
            "picture": CImageFactory().image.url,
            "invoice": CFileFactory().file.url,
            "content": fake.text(max_nb_chars=150),
            "written": fake.date(),
            "price": fake.pyfloat(positive=True, left_digits=2, right_digits=2),
        }
        obj.save()
        if groups:
            obj.groups.add(*groups)
            obj.save()
        return obj


class CPageFactory(factory.django.DjangoModelFactory):
    """Factory for CPage."""

    class Meta:
        model = models.CPage

    title = "Super title"
    description = "Nice description"
    template = "template.html"
    slug = ""
    parent = None

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        obj = model_class(*args, **kwargs)
        obj.slug = slugify(obj.title)
        if not kwargs.get("lang"):
            obj.lang = Settings.objects.first().primary_language
        obj.save()
        return obj


class CThemeFactory(factory.django.DjangoModelFactory):
    """Factory for CTheme."""

    class Meta:
        model = models.CTheme

    templates = ["template_1.html", "template_2.html"]
