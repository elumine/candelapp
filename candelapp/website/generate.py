"""Candelapp website generation tool."""
import copy
from datetime import datetime
from os import listdir, makedirs
from pytz import timezone
from requests.compat import urljoin
import shutil
import socket
import subprocess
import tempfile
from urllib import parse
from xml.etree import cElementTree as ET
import zipfile

from django.conf import settings
from django.utils.translation import gettext_lazy as _

import ftplib
from jinja2 import Environment, FileSystemLoader
from jinja2 import exceptions as jinja_exceptions
from jinja2_custom_filters_extension.string_filters_extension import (
    StringFilterExtension,
)

from candelapp.settings.models import Settings
from candelapp.tools.encryption import PasswordManager
from candelapp.website import models
from candelapp.website.filters import limit, shuffle, to_list
from candelapp.website.serializers import CModelItemSerializer
from candelapp.website import tools


TZ = timezone(settings.TIME_ZONE)


ERRORS_LIST = {
    "error_ftp_connection": _("Cannot connect to the FTP server. Please check your settings."),
    "error_ftp_action": _(
        "A problem occurred when sending the generated site. Please check the FTP rights."
    ),
    "improperly_configured_ftp": _("The FTP settings are not properly configured."),
    "improperly_configured_names": _("The site name and address are not configured."),
    "render_syntax_error": _("There is a syntax problem in the template."),
    "render_undefined_error": _("A variable is used and does not exist."),
    "render_template_error": ("These templates are referenced but do not exist."),
    "no_page": _("You need at least one page to generate the site."),
    "no_theme": _("No themes have been imported yet."),
    "missing_templates": _(
        "This or these templates are used in pages but do not exist in the current theme:"
    ),
}

DEFAULT_CONTEXT = {
    "site_name": "",
    "title": "",
    "description": "",
    "lang": "",
}


class GenerationException(Exception):
    """Exception during a website generation."""

    def __init__(self, message: str = ""):
        """Init GenerationException."""
        self.message = message


class WebsiteGenerator:
    """Tool to generate website according the data."""

    def __init__(self):
        """Setup initial data."""
        self.settings = Settings.objects.first()

    def validate(self):
        """Check that data is reliable before generating."""
        # Check theme existence
        self.theme = models.CTheme.objects.first()

        if not all(
            [
                bool(self.settings.site_name),
                bool(self.settings.site_url),
            ]
        ):
            raise GenerationException(ERRORS_LIST["improperly_configured_names"])
        if not self.theme:
            raise GenerationException(ERRORS_LIST["no_theme"])

        # Check that FTP is configured before going further
        if not all(
            [
                bool(self.settings.ftp_host),
                bool(self.settings.ftp_user),
                bool(self.settings.ftp_password),
            ]
        ):
            raise GenerationException(ERRORS_LIST["improperly_configured_ftp"])
        pm = PasswordManager()
        self.ftp_password = pm.decrypt(self.settings.ftp_password)

        # Check that FTP is reachable with set configuration
        try:
            ftp = ftplib.FTP()
            ftp.connect(self.settings.ftp_host, self.settings.ftp_port)
            ftp.login(user=self.settings.ftp_user, passwd=self.ftp_password)
        except (ftplib.error_perm, socket.gaierror, OSError):
            raise GenerationException(ERRORS_LIST["error_ftp_connection"])

        # Templates check
        page_templates = models.CPage.objects.values_list("template", flat=True)

        if page_templates.count() == 0:
            raise GenerationException(ERRORS_LIST["no_page"])

        # If all the templates used in pages are not in the templates list
        # from the theme. There will be a difference with all the templates missing
        differences = set(page_templates) - set(self.theme.templates)
        if len(differences) != 0:
            raise GenerationException(
                "{} {}".format(ERRORS_LIST["missing_templates"], [t for t in differences])
            )

    def prepare_data(self):
        """Get necessary data."""
        self.pages = models.CPage.objects.all()

        self.modelitems_groupby_model = models.CModelItem.objects.group_modelitems_by_model()

        # Sitemap
        xmlns = "http://www.sitemaps.org/schemas/sitemap/0.9"
        self.sitemap = ET.Element("urlset")
        self.sitemap.attrib["xmlns"] = xmlns

        # Sitemapindex
        self.sitemapindex = ET.Element("sitemapindex")
        self.sitemapindex.attrib["xmlns"] = xmlns
        element = ET.SubElement(self.sitemapindex, "sitemap")
        ET.SubElement(element, "loc").text = urljoin(self.settings.site_url, "sitemap.xml")
        ET.SubElement(element, "lastmod").text = datetime.now().astimezone(TZ).isoformat()

    def add_entry_sitemap(self, modified, url):
        """Add an entry in the sitemap."""
        element = ET.SubElement(self.sitemap, "url")
        ET.SubElement(element, "loc").text = urljoin(self.settings.site_url, url)
        # Use timezone isoformat to match the w3c datetime format for sitemaps
        ET.SubElement(element, "lastmod").text = modified.astimezone(TZ).isoformat()

    def render_html(self, page, context):
        """Render html from context and template."""
        try:
            template = self.env.get_template(page.template)
            html = template.render(context)
        except jinja_exceptions.TemplateSyntaxError as e:
            raise GenerationException(
                "{} {}. ({})".format(ERRORS_LIST["render_syntax_error"], page.template, e.message)
            )
        except (
            jinja_exceptions.TemplateNotFound,
            jinja_exceptions.TemplatesNotFound,
        ) as e:
            raise GenerationException(
                "{}: {}".format(ERRORS_LIST["render_template_error"], e.message)
            )
        except jinja_exceptions.UndefinedError as e:
            raise GenerationException(
                "{} {}. ({})".format(
                    ERRORS_LIST["render_undefined_error"], page.template, e.message
                )
            )
        return html

    def generate_single_pages(self, rootdir):
        """Update context and generate each pages without model."""
        for page in self.pages.filter(model__isnull=True):
            # Context
            context = copy.deepcopy(DEFAULT_CONTEXT)
            context["lang"] = page.lang.code
            context["site_name"] = self.settings.site_name
            context["title"] = page.title
            context["description"] = page.description
            context["created"] = page.created
            context["modified"] = page.modified
            modelitems = self.modelitems_groupby_model.get(page.lang.code)
            if modelitems:
                context.update(modelitems)

            # Page family to context
            family_list = page.get_family()
            context["family_urls"] = tools.build_family_urls_with_lang_list(
                family_list, self.settings.primary_language
            )

            # Render html
            html = self.render_html(page, context)

            url = tools.build_page_url(page, self.settings.primary_language)
            pagedirs = f"{rootdir}/{url}"

            # Add entry to sitemap
            self.add_entry_sitemap(page.modified, url)

            # Make dirs and create file
            try:
                makedirs(pagedirs)
            except FileExistsError:
                pass
            with open(f"{pagedirs}/index.html", "w") as htmlfile:
                htmlfile.write(html)

    def generate_multiple_pages(self, rootdir):
        """Update context and generate each pages associated to a model."""
        for page in self.pages.filter(model__isnull=False):
            for modelitem in page.model.cmodelitem_set.filter(lang=page.lang):
                modelitem_serialized = CModelItemSerializer(modelitem)
                modelitem_object = modelitem_serialized.data
                # Context
                # Put all the fields outside data in the same level
                context = copy.deepcopy(DEFAULT_CONTEXT)
                context["lang"] = page.lang.code
                context["site_name"] = self.settings.site_name
                context["title"] = modelitem_object["data"]["title"]
                context["description"] = modelitem_object["data"]["description"]
                context["created"] = modelitem.created
                context["modified"] = modelitem.modified
                modelitems = self.modelitems_groupby_model.get(page.lang.code)
                if modelitems:
                    context.update(modelitems)
                context.update({"object": modelitem_object})

                # Modelitems family to context
                family_list = modelitem.get_family()
                context["family_urls"] = tools.build_family_urls_with_lang_list(
                    family_list, self.settings.primary_language
                )

                # Add entry to sitemap
                self.add_entry_sitemap(modelitem.modified, modelitem.url)

                # Render html
                html = self.render_html(page, context)

                # Always try to create directories
                dirs = f"{rootdir}{modelitem.url}"
                try:
                    makedirs(dirs)
                except FileExistsError:
                    pass

                with open(f"{dirs}/index.html", "w") as htmlfile:
                    htmlfile.write(html)

    def generate_website(self, tempdir):
        """Utility function to check and generate the pages then send it to server."""
        self.validate()

        self.prepare_data()

        with zipfile.ZipFile(self.theme.file, "r") as archive:
            with tempfile.TemporaryDirectory() as tempthemedir:
                archive.extractall(tempthemedir)
                file_loader = FileSystemLoader(f"{tempthemedir}/templates/")

                #  StringFilterExtension for slug
                self.env = Environment(
                    loader=file_loader,
                    extensions=[StringFilterExtension],
                )
                self.env.filters.update({"limit": limit, "shuffle": shuffle, "to_list": to_list})

                self.generate_single_pages(tempdir)

                self.generate_multiple_pages(tempdir)

                # Write sitemap
                sitemap_tree = ET.ElementTree(self.sitemap)
                sitemap_tree.write(
                    f"{tempdir}/sitemap.xml",
                    encoding="utf-8",
                    xml_declaration=True,
                )

                # Write sitemap_index
                sitemapindex_tree = ET.ElementTree(self.sitemapindex)
                sitemapindex_tree.write(
                    f"{tempdir}/sitemapindex.xml",
                    encoding="utf-8",
                    xml_declaration=True,
                )

                # Robots.txt
                robots_tpl = "Sitemap: {}\n\n" "User-Agent: *\n" "Host: {}"
                txt = robots_tpl.format(
                    urljoin(self.settings.site_url, "sitemapindex.xml"),
                    self.settings.site_url,
                )
                with open(f"{tempdir}/robots.txt", "w") as robots:
                    robots.write(txt)

                # Copy assets directory if exists to temp one
                if "assets" in listdir(tempthemedir):
                    shutil.copytree(
                        f"{tempthemedir}/assets",
                        f"{tempdir}/assets",
                    )

    def ftp_command(self, sitedir):
        """
        FTP commands with lftp from system.

        Lftp mirror is by default remote to source.
        -R reverses the link from source to remote.
        -e deletes remote files that doesnt exist anymore in source
        -P parallizes

        """
        cmd = f"mirror -P 2 -Re {sitedir}/ candelapp/"
        wrap_cmd = f"set ftp:ssl-allow no; {cmd}; quit"

        # Escape special characters in password
        password = parse.quote_plus(self.ftp_password)

        connection_infos = f"ftp://{self.settings.ftp_user}:{password}@{self.settings.ftp_host}"
        lftp_cmd = [
            "lftp",
            connection_infos,
            "-e",
            wrap_cmd,
        ]

        call = subprocess.call(lftp_cmd)
        if bool(call) is True:
            raise GenerationException(ERRORS_LIST["error_ftp_action"])
