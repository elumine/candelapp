from django.core.exceptions import ValidationError as DjangoValidationError

from rest_framework.exceptions import ValidationError


class DRFValidationMixin:
    """
    DRFValidationMixin does help to enforce the validation of the model
    with the exact same serializer validating the data.

    when a model is saved using ORM or the admin, we don't want a different
    behaviour than the API validation.

    In addition, this allows the validation to be performed only once,
    regardless of whether the object is created or updated. The validation
    logic resides in one place.

    In the save method of a serializer, "drf_validated" should be set to True
    before firing the create or update.
    """

    drf_validated = False
    serializer_class = None

    def __init__(self, *args, **kwargs):
        """
        Set drf_validated if the save method is called by a serializer
        and remove the kwargs attribute.
        """

        from candelapp.website.models import CModel, CModelItem

        self.cmodel_class = CModel
        self.cmodelitem_class = CModelItem

        if self.serializer_class is None:
            raise NotImplementedError("A serializer class must be set.")

        if "drf_validated" in kwargs:
            self.drf_validated = kwargs.pop("drf_validated")
        super().__init__(*args, **kwargs)

    def drf_clean_object(self):
        """Force drf validation when not using the drf views."""
        serializer = self.serializer_class(self)
        data = serializer.data
        db_instance = None
        context = {}
        if data.get("pk"):
            db_instance = self.__class__.objects.get(pk=data["pk"])
        if self.__class__ == self.cmodelitem_class:
            context = {"model_attribute": data["model"]}
        serializer = self.serializer_class(db_instance, data=data, context=context)
        serializer.is_valid(raise_exception=True, raise_django_exception=True)
        return serializer.validated_data

    def save(self, *args, **kwargs):
        """
        If the save has been fired by a serializer,
        don't validate again the data in the model.
        """
        if self.drf_validated is False:
            validated_data = self.drf_clean_object()

            # For the CModel, the name is not set by the serializer
            # update it after the validation
            if isinstance(self, self.cmodel_class) and not self.pk:
                self.name = validated_data["name"]

        super().save(*args, **kwargs)


class DjangoExceptionMixin:
    def is_valid(self, raise_exception=False, raise_django_exception=False):
        assert not hasattr(self, "restore_object"), (
            "Serializer `%s.%s` has old-style version 2 `.restore_object()` "
            "that is no longer compatible with REST framework 3. "
            "Use the new-style `.create()` and `.update()` methods instead."
            % (self.__class__.__module__, self.__class__.__name__)
        )

        assert hasattr(self, "initial_data"), (
            "Cannot call `.is_valid()` as no `data=` keyword argument was "
            "passed when instantiating the serializer instance."
        )

        if not hasattr(self, "_validated_data"):
            try:
                self._validated_data = self.run_validation(self.initial_data)
            except ValidationError as exc:
                self._validated_data = {}
                self._errors = exc.detail
            else:
                self._errors = {}

        if self._errors and raise_exception and not raise_django_exception:
            raise ValidationError(self.errors)
        if self._errors and raise_exception and raise_django_exception:
            error_dict = {}
            for key, value in self.errors.items():
                # In case of error field in the model fields
                # use depth 1
                if isinstance(value, dict):
                    for k, v in value.items():
                        error_dict[k] = v
                else:
                    error_dict[key] = [str(v) for v in value]
            raise DjangoValidationError(error_dict)
        return not bool(self._errors)
