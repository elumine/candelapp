"""Website models."""
from itertools import groupby
from operator import itemgetter

from django.core.exceptions import ValidationError
from django.db import connection, models
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _

from django.contrib.auth.models import Group
from django_extensions.db.models import TimeStampedModel

from candelapp.website import constants
from candelapp.website import mixins


class CModelCategory(models.Model):
    """A model for model categories."""

    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = _("Model category")
        verbose_name_plural = _("Model categories")

    def __str__(self):
        return self.name


class CModel(mixins.DRFValidationMixin, models.Model):
    """Represents models in candelapp."""

    name = models.CharField(max_length=100, unique=True)
    verbose_name = models.CharField(max_length=100)
    category = models.ForeignKey(CModelCategory, null=True, on_delete=models.SET_NULL)
    fields = models.JSONField()
    type = models.CharField(choices=constants.MODEL_TYPES, default="simple", max_length=30)

    # # permissions
    can_add = models.JSONField(
        help_text=_("Determines who can add the objects created from this model"), default=dict
    )
    can_edit = models.JSONField(
        help_text=_("Determines who can edit the objects created from this model"), default=dict
    )
    can_delete = models.JSONField(
        help_text=_("Determines who can delete the objects created from this model"), default=dict
    )
    can_view = models.JSONField(
        help_text=_("Determines who can see the objects created from this model"), default=dict
    )

    is_public = models.BooleanField(
        help_text=_("Modelization can be seen by anyone"), default=False
    )

    class Meta:
        verbose_name = _("Candelapp model")
        verbose_name_plural = _("Candelapp models")

    def __str__(self):
        return self.name

    def __init__(self, *args, **kwargs):
        """Set serializer_class here to avoid circular import."""
        from candelapp.website.serializers import CModelSerializer

        self.serializer_class = CModelSerializer

        super().__init__(*args, **kwargs)


class CModelItemManager(models.Manager):
    """Add extra methods to CModel manager."""

    def group_modelitems_by_model(self):
        """
        Group all modelitems by lang and by model in a dict.

        A simple model will be a dict per lang instead of a list.

        Output example:
        {
            "en": {
                "model1": [],
                "model2": [],
                "model3": {}
                },
            "fr": {
                "model1": [],
                "model3": {}
            }
        }
        """

        qs = self.order_by("lang__code", "model__name").values(
            "lang__code",
            "model__name",
            "model__type",
            "created",
            "modified",
            "url",
            "data",
        )

        grouped_modelitems = {}

        # First group all qs items by lang code
        groupby_lang = groupby(qs, itemgetter("lang__code"))

        for lang, items_by_lang in groupby_lang:
            grouped_modelitems[lang] = {}
            # Group items (grouped by lang) by model
            groupby_model = groupby(items_by_lang, itemgetter("model__name"))
            for model, items_by_model in groupby_model:
                # Replace dashes from slugified model name with underscores
                # for jinja2
                model_name = model.replace("-", "_")
                modelitems_list = list(items_by_model)
                # A simple model can't have multiple items
                if modelitems_list[0]["model__type"] == "multiple":
                    grouped_modelitems[lang][model_name] = modelitems_list
                else:
                    grouped_modelitems[lang][model_name] = modelitems_list[0]

        return grouped_modelitems


class CModelItem(mixins.DRFValidationMixin, TimeStampedModel):
    """
    Represents an object based on a CModel.

    Parent is the reference of the primary language modelitem when
    multiple langs are set.
    Non-primary languages must have a reference to this parent.
    """

    data = models.JSONField()
    model = models.ForeignKey(CModel, on_delete=models.CASCADE)
    groups = models.ManyToManyField(Group, blank=True)

    lang = models.ForeignKey("international.Language", on_delete=models.PROTECT)
    status = models.CharField(
        _("Status"), choices=constants.STATUSES, default="draft", max_length=30
    )
    parent = models.ForeignKey("self", null=True, on_delete=models.SET_NULL)

    url = models.CharField(blank=True, max_length=250)

    objects = CModelItemManager()

    class Meta:
        verbose_name = _("Candelapp object")
        verbose_name_plural = _("Candelapp objects")

    def __str__(self):
        return self.data.get("title")

    def __init__(self, *args, **kwargs):
        """Set serializer_class here to avoid circular import."""
        from candelapp.website.serializers import CModelItemSerializer

        self.serializer_class = CModelItemSerializer

        super().__init__(*args, **kwargs)

    @property
    def translated_status(self):
        return constants.DICT_STATUSES.get(self.status)

    def get_family(self):
        """Get all siblings and parent."""
        filtered_qs = CModelItem.objects.filter(model=self.model)

        if self.parent is None:
            qs = filtered_qs.filter(parent=self)
            parent = self
        else:
            qs = filtered_qs.filter(parent=self.parent)
            parent = self.parent

        siblings = list(qs)
        if len(siblings) != 0:
            # Only add parent if qs has results
            siblings.append(parent)

        return siblings

    def clean(self):
        """Only one parent can exist in a simple model."""
        if not self.pk and self.model.type == "simple" and self.parent is None:
            if CModelItem.objects.filter(parent=None, model=self.model).count() >= 1:
                raise ValidationError(_("A simple model can only have one parent item."))

    def save(self, *args, **kwargs):
        """
        Update serializer_class when updating an instance.
        Each time a modelitem is saved, re-generate its url.
        """
        self.clean()

        from candelapp.website.tools import build_modelitem_url

        self.url = build_modelitem_url(self.model, self.lang, self.data["title"])
        return super().save(*args, **kwargs)


class CPage(TimeStampedModel):
    """
    Page model.

    Parent is the reference of the primary language page when multiple langs are set.
    Non-primary languages must have a reference to this parent.
    """

    title = models.CharField(_("Title"), max_length=60)
    description = models.CharField(_("Description"), blank=True, max_length=150)

    status = models.CharField(
        _("Status"), choices=constants.STATUSES, default="draft", max_length=30
    )
    template = models.CharField(_("Template name"), max_length=255)
    model = models.ForeignKey(
        CModel,
        null=True,
        on_delete=models.SET_NULL,
    )
    slug = models.CharField(_("Slug"), max_length=60)
    is_index = models.BooleanField(_("Home page"), default=False)

    lang = models.ForeignKey("international.Language", on_delete=models.PROTECT)

    parent = models.ForeignKey("self", null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name = _("Candelapp page")
        verbose_name_plural = _("Candelapp pages")

    def clean(self):
        """
        Children pages must have the same model as parents.

        A model can't be reused by another parent page and its children.
        """
        if self.parent:
            if self.parent.model != self.model:
                raise ValidationError(_("A child must have the same parent's model."))
        else:
            if self.model:
                qs = CPage.objects.filter(model=self.model, parent=None)
                if self.pk:
                    if qs.exclude(pk=self.pk).exists():
                        raise ValidationError(_("A model is unique for a parent and its children."))
                else:
                    if qs.exists():
                        raise ValidationError(_("A model is unique for a parent and its children."))
        if self.model and self.model.type == "simple":
            raise ValidationError(_("A page cannot be tied to a simple model."))

    def get_family(self):
        """Get all siblings and parent."""
        if self.parent is None:
            qs = CPage.objects.filter(parent=self)
            parent = self
        else:
            qs = CPage.objects.filter(parent=self.parent)
            parent = self.parent

        siblings = list(qs)
        if len(siblings) != 0:
            # Only add parent if qs has results
            siblings.append(parent)

        return siblings

    def save(self, *args, **kwargs):
        """
        Clean before save.

        Also slugify page title when needed.
        """
        self.clean()
        if self.title is not None:
            self.slug = slugify(self.title)
        return super().save(*args, **kwargs)


def tenant_images_folder(instance, filename):
    return "{}/{}/{}".format(connection.tenant.name, "images", filename)


class CImage(models.Model):
    """Image model."""

    image = models.ImageField(upload_to=tenant_images_folder)

    class Meta:
        verbose_name = _("Image")
        verbose_name_plural = _("Images")


def tenant_files_folder(instance, filename):
    return "{}/{}/{}".format(connection.tenant.name, "files", filename)


class CFile(models.Model):
    """File model."""

    file = models.FileField(upload_to=tenant_files_folder)

    class Meta:
        verbose_name = _("File")
        verbose_name_plural = _("Files")


def tenant_theme_folder(instance, filename):
    return "{}/{}/{}".format(connection.tenant.name, "theme", "theme.zip")


class CTheme(TimeStampedModel):
    """Represents a theme for candelapp."""

    file = models.FileField(null=True, upload_to=tenant_theme_folder)
    templates = models.JSONField()

    class Meta:
        verbose_name = _("Theme")

    def save(self, *args, **kwargs):
        """Restrain to only one theme."""
        if self.__class__.objects.count():
            self.pk = self.__class__.objects.first().pk
        super().save(*args, **kwargs)
