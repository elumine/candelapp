"""
Website related signals.

When updating a modelitem, call modelitem save() method to use
build_modelitem_url to rebuild the url
that makes the modelitem reachable from templates.
But to do so, a modelitem needs its model to be linked to a page.
If a page is created, modified or deleted, modelitems won't rebuild
their urls by themselves. These signals handle this process.
"""
from typing import Type

from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from candelapp.website.models import CPage, CModelItem


def update_links(page, deleting=False):
    # Retrieve all objects that have their model linked to the page
    modelitems = CModelItem.objects.filter(lang=page.lang, model=page.model)

    if not modelitems:
        return

    for item in modelitems:
        item.save(update_fields=["url"])


@receiver(post_save, sender=CPage)
def after_save_handler(sender: Type[CPage], instance: CPage, created: bool, **kwargs) -> None:
    """When saving a CPage instance, ensure related modelitems have links updated."""
    update_links(instance)


@receiver(post_delete, sender=CPage)
def after_delete_handler(sender: Type[CPage], instance: CPage, **kwargs) -> None:
    """When saving a CPage instance, ensure related modelitems have links updated."""
    update_links(instance, deleting=True)
