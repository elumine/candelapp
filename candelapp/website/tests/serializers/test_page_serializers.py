"""Page serializer tests."""
import copy
import pytz

from django.conf import settings

from rest_framework.exceptions import ErrorDetail

from candelapp.international.models import Language
from candelapp.settings.models import Settings
from candelapp.website import factories
from candelapp.website import models
from candelapp.website import serializers

from candelapp.tools import test_utils


TZ = pytz.timezone(settings.TIME_ZONE)


class CPageCreateSerializerTest(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):

    """Test CPageCreateSerializer."""

    @classmethod
    def setup_class(cls):
        cls.settings = Settings.objects.first()
        cls.primary_lang = Settings.objects.first().primary_language
        cls.cv_lang = Language.objects.get(code="cv")
        cls.es_lang = Language.objects.get(code="es")
        cls.settings.secondary_languages.add(cls.cv_lang, cls.es_lang)

    def test_create_serializer_errors(self):
        """Test createserializer errors."""

        # No data
        serializer = serializers.CPageCreateSerializer(data={})
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "template": [ErrorDetail(string="This field is required.", code="required")],
            "title": [ErrorDetail(string="This field is required.", code="required")],
            "lang": [ErrorDetail(string="This field is required.", code="required")],
        }

        # Theme doesn't exist
        serializer = serializers.CPageCreateSerializer(
            data={
                "type": "simple",
                "title": "Title",
                "template": "test.html",
                "lang": self.primary_lang.pk,
            }
        )

        assert serializer.is_valid() is False
        assert serializer.errors == {
            "template": [ErrorDetail(string="This template does not exist.", code="invalid")]
        }

        # Theme exists but not the template
        factories.CThemeFactory(templates=["template.html"])

        serializer = serializers.CPageCreateSerializer(
            data={
                "title": "Title",
                "template": "test.html",
                "lang": self.primary_lang.pk,
            }
        )
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "template": [ErrorDetail(string="This template does not exist.", code="invalid")]
        }

        # A page without a model needs a description
        serializer = serializers.CPageCreateSerializer(
            data={
                "title": "Title",
                "template": "template.html",
                "lang": self.primary_lang.pk,
            }
        )
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "description": [
                ErrorDetail(
                    string="A description is required for this page type.",
                    code="invalid",
                )
            ]
        }

        # A page needs a lang
        serializer = serializers.CPageCreateSerializer(
            data={
                "type": "simple",
                "title": "Title",
                "template": "template.html",
                "description": "a smol description",
            }
        )
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "lang": [
                ErrorDetail(
                    string="This field is required.",
                    code="required",
                )
            ]
        }

        # A parent page needs the primary language as lang
        serializer = serializers.CPageCreateSerializer(
            data={
                "title": "Title",
                "template": "template.html",
                "description": "a smol description",
                "lang": self.cv_lang.pk,
            }
        )
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "lang": [
                ErrorDetail(
                    string="The language of a parent must be the main language.",
                    code="invalid",
                )
            ]
        }

        # A page needs a lang that has been set in settings
        serializer = serializers.CPageCreateSerializer(
            data={
                "title": "Title",
                "template": "template.html",
                "description": "a smol description",
                "lang": 11,
            }
        )
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "lang": [
                ErrorDetail(
                    string="This language does not exist or is not available.",
                    code="invalid",
                )
            ]
        }

        # A page with a model can't be the index
        model = factories.CModelFactory()
        serializer = serializers.CPageCreateSerializer(
            data={
                "title": "Title",
                "template": "template.html",
                "model": model.pk,
                "lang": self.primary_lang.pk,
                "is_index": True,
            }
        )
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "is_index": [
                ErrorDetail(
                    string="A page with a model set cannot be the index.",
                    code="invalid",
                )
            ]
        }

        parent_page = factories.CPageFactory(lang=self.primary_lang)

        # A child page can't set is_index nor model itself
        model = factories.CModelFactory()
        data = {
            "title": "Title",
            "template": "template.html",
            "description": "a smol description",
            "lang": self.cv_lang.pk,
            "parent": parent_page.pk,
        }
        fields_to_test = [{"model": model.pk}, {"is_index": True}]
        for field in fields_to_test:
            payload = copy.copy(data)
            payload.update(field)
            serializer = serializers.CPageCreateSerializer(data=payload)
            assert serializer.is_valid() is False
            assert serializer.errors == {
                "non_field_errors": [
                    ErrorDetail(
                        string="A child page cannot update the model nor the index status.",
                        code="invalid",
                    )
                ]
            }

        # A page with a parent must not have a secondary language already used as lang
        child_page = factories.CPageFactory(parent=parent_page, lang=self.es_lang)
        serializer = serializers.CPageCreateSerializer(
            data={
                "title": "Title",
                "template": "template.html",
                "description": "a smol description",
                "lang": child_page.lang.pk,
                "parent": parent_page.pk,
            }
        )
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "lang": [
                ErrorDetail(
                    string="The language of a child cannot be used by "
                    "another child or be the main language.",
                    code="invalid",
                )
            ]
        }

        # A page with a parent must not have the primary language as lang
        serializer = serializers.CPageCreateSerializer(
            data={
                "title": "Title",
                "template": "template.html",
                "description": "a smol description",
                "lang": parent_page.lang.pk,
                "parent": parent_page.pk,
            }
        )

        assert serializer.is_valid() is False
        assert serializer.errors == {
            "lang": [
                ErrorDetail(
                    string=(
                        "The language of a child cannot "
                        "be used by another child or be the main language."
                    ),
                    code="invalid",
                )
            ]
        }

        # Reuse of the model is not allowed
        model = factories.CModelFactory(type="multiple")
        factories.CPageFactory(model=model)
        serializer = serializers.CPageCreateSerializer(
            data={
                "title": "Title",
                "template": "template.html",
                "model": model.pk,
                "lang": self.primary_lang.pk,
            }
        )
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "non_field_errors": [
                ErrorDetail(
                    string=("The model set is already in use."),
                    code="invalid",
                )
            ]
        }

    def test_create_serializer(self):
        """Test createserializerpage."""
        factories.CThemeFactory(templates=["template.html"])

        # Simple page
        serializer = serializers.CPageCreateSerializer(
            data={
                "title": "Title of storm",
                "description": "smol desc",
                "template": "template.html",
                "lang": self.primary_lang.pk,
            }
        )
        assert serializer.is_valid() is True
        instance = serializer.save()

        assert models.CPage.objects.count() == 1
        page = models.CPage.objects.first()
        assert instance.slug == "title-of-storm"

        # Model multiple page
        model = factories.CModelFactory(type="multiple")
        serializer = serializers.CPageCreateSerializer(
            data={
                "title": "Title",
                "template": "template.html",
                "model": model.pk,
                "lang": self.primary_lang.pk,
            }
        )
        assert serializer.is_valid() is True
        instance = serializer.save()

        assert models.CPage.objects.count() == 2

        assert instance.model == model

        # Child of page
        parent = factories.CPageFactory(lang=self.primary_lang)
        serializer = serializers.CPageCreateSerializer(
            data={
                "title": "Title",
                "description": "hello",
                "template": "template.html",
                "lang": self.cv_lang.pk,
                "parent": parent.pk,
            }
        )
        assert serializer.is_valid() is True
        instance = serializer.save()
        assert instance.parent == parent

        # Child of page with a model
        model = factories.CModelFactory(type="multiple")
        parent = factories.CPageFactory(lang=self.primary_lang, model=model)
        serializer = serializers.CPageCreateSerializer(
            data={
                "title": "Title",
                "template": "template.html",
                "lang": self.cv_lang.pk,
                "parent": parent.pk,
            }
        )
        assert serializer.is_valid() is True
        instance = serializer.save()
        assert instance.parent == parent
        assert instance.model == model

        # Only one page can be the index
        page = factories.CPageFactory(is_index=True)
        serializer = serializers.CPageCreateSerializer(
            data={
                "title": "Title",
                "type": "simple",
                "description": "smol description",
                "template": "template.html",
                "lang": self.primary_lang.pk,
                "is_index": True,
            }
        )
        assert serializer.is_valid() is True

        instance = serializer.save()

        assert instance.is_index is True
        page.refresh_from_db()
        assert page.is_index is False


class CPageUpdateSerializerTest(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):

    """Test CPageUpdateSerializer."""

    @classmethod
    def setup_class(cls):
        cls.settings = Settings.objects.first()
        cls.primary_lang = Settings.objects.first().primary_language
        cls.cv_lang = Language.objects.get(code="cv")
        cls.es_lang = Language.objects.get(code="es")
        cls.settings.secondary_languages.add(cls.cv_lang, cls.es_lang)

    def test_update_serializer_errors(self):
        """Test updateserializerpage errors."""

        # No data
        serializer = serializers.CPageUpdateSerializer(data={})
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "template": [ErrorDetail(string="This field is required.", code="required")],
            "title": [ErrorDetail(string="This field is required.", code="required")],
        }

        # Theme doesn't exist
        serializer = serializers.CPageUpdateSerializer(
            data={
                "title": "Title",
                "template": "test.html",
            }
        )
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "template": [ErrorDetail(string="This template does not exist.", code="invalid")]
        }

        # A child page can't update is_index nor model itself
        factories.CThemeFactory(templates=["template.html"])
        model = factories.CModelFactory()
        parent = factories.CPageFactory()
        page = factories.CPageFactory(parent=parent)
        data = {
            "title": "Title",
            "template": "template.html",
            "description": "a smol description",
        }
        fields_to_test = [{"model": model.pk}, {"is_index": True}]
        for field in fields_to_test:
            payload = copy.copy(data)
            payload.update(field)
            serializer = serializers.CPageUpdateSerializer(data=payload, instance=page)
            assert serializer.is_valid() is False
            assert serializer.errors == {
                "non_field_errors": [
                    ErrorDetail(
                        string=("A child page cannot update the model nor the index status."),
                        code="invalid",
                    )
                ]
            }

        # A page without a model can't be without description
        page = factories.CPageFactory()
        serializer = serializers.CPageUpdateSerializer(
            data={
                "title": "Title",
                "template": "template.html",
            },
            instance=page,
        )
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "description": [
                ErrorDetail(
                    string="A description is required for this page type.",
                    code="invalid",
                )
            ]
        }

        # Lang can't be changed so is ignored
        serializer = serializers.CPageUpdateSerializer(
            data={
                "title": "Title",
                "description": "desc",
                "template": "template.html",
                "lang": 23098,
            },
            instance=page,
        )
        assert serializer.is_valid() is True
        instance = serializer.save()

        assert instance.lang.pk != 23098

        # A page with a model can't update as the index
        page = factories.CPageFactory(is_index=True)
        serializer = serializers.CPageUpdateSerializer(
            page,
            data={
                "title": "Title",
                "model": model.pk,
                "template": "template.html",
            },
        )
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "is_index": [
                ErrorDetail(
                    string="A page with a model set cannot be the index.",
                    code="invalid",
                )
            ]
        }

        # Reuse of the model is not allowed
        model = factories.CModelFactory(type="multiple")
        another_model = factories.CModelFactory(type="multiple")
        factories.CPageFactory(model=model)
        page = factories.CPageFactory(model=another_model)
        serializer = serializers.CPageUpdateSerializer(
            page,
            data={
                "description": "desc",
                "title": "Title",
                "template": "template.html",
                "model": model.pk,
                "lang": self.primary_lang.pk,
            },
        )
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "non_field_errors": [
                ErrorDetail(
                    string=("The model set is already in use."),
                    code="invalid",
                )
            ]
        }


class CPageReadSerializerTest(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    """Test CPageReadSerializer."""

    @classmethod
    def setup_class(cls):
        cls.settings = Settings.objects.first()
        cls.primary_lang = Settings.objects.first().primary_language

    def test_read_serializer(self):
        """Test pagereadserializer."""
        # Page with no model
        page = factories.CPageFactory(
            title="This is a simple page",
            description="With a simple description",
            lang=self.primary_lang,
            template="template.html",
        )

        serializer = serializers.CPageReadSerializer(page)
        assert serializer.data == {
            "created": page.created.astimezone(TZ).isoformat(),
            "description": "With a simple description",
            "is_index": False,
            "model": None,
            "model_name": None,
            "modified": page.modified.astimezone(TZ).isoformat(),
            "pk": page.pk,
            "slug": "this-is-a-simple-page",
            "status": "draft",
            "template": "template.html",
            "title": "This is a simple page",
            "lang": self.primary_lang.pk,
            "parent": None,
        }

        # Page with a model
        model = factories.CModelFactory(verbose_name="a-model", type="multiple")
        page = factories.CPageFactory(
            title="This is a page with a model",
            lang=self.primary_lang,
            template="template.html",
            model=model,
            description="",
        )
        serializer = serializers.CPageReadSerializer(page)
        assert serializer.data == {
            "created": page.created.astimezone(TZ).isoformat(),
            "description": "",
            "is_index": False,
            "model": model.pk,
            "model_name": model.name,
            "modified": page.modified.astimezone(TZ).isoformat(),
            "pk": page.pk,
            "slug": "this-is-a-page-with-a-model",
            "status": "draft",
            "template": "template.html",
            "title": "This is a page with a model",
            "lang": self.primary_lang.pk,
            "parent": None,
        }
