"""CFile api tests."""
from django.db import connection

from candelapp.authentication.factories import AdminFactory, UserFactory
from candelapp.website import models, factories
from candelapp.tools import test_utils


ENDPOINTS = {
    "files": "/api/v1/website/files/",
}


class CFileAPITestCase(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    """CFile endpoint testcases."""

    @classmethod
    def setUpTestData(cls):
        """Set up users."""
        super().setUpTestData()
        cls.admin_user = AdminFactory()
        cls.non_admin_user = UserFactory()
        cls.file = factories.CFileFactory()

    def test_send_with_admin_user(self):
        """Try to get with a admin user."""
        self._test_get(
            "{}{}/".format(ENDPOINTS["files"], self.file.pk),
            self.admin_user,
            200,
        )

    def test_send_with_non_admin_user(self):
        """Try to get with a non admin user."""
        response = self._test_get(
            "{}{}/".format(ENDPOINTS["files"], self.file.pk),
            self.non_admin_user,
            403,
        )
        assert response.json() == {"detail": "You do not have permission to perform this action."}

    def test_not_found_file(self):
        """Try to get an unknown model."""
        self._test_get(
            "{}{}/".format(ENDPOINTS["files"], 293820938),
            self.admin_user,
            404,
        )

    def test_forbidden_methods(self):
        """Try to PATCH, DELETE files."""
        data = {"file": test_utils.get_gif()}
        self._test_patch(
            "{}{}/".format(ENDPOINTS["files"], self.file.pk),
            self.admin_user,
            405,
            data,
        )
        self._test_delete(
            "{}{}/".format(ENDPOINTS["files"], self.file.pk),
            self.admin_user,
            405,
        )

    def test_post_file(self):
        """Try to POST a file."""
        data = {"file": test_utils.get_pdf()}
        self._test_post(
            ENDPOINTS["files"],
            self.admin_user,
            201,
            data,
        )

        assert models.CFile.objects.count() == 2

        cfile = models.CFile.objects.last()
        assert "{}".format(connection.tenant.name) in cfile.file.url
        assert ".pdf" in cfile.file.url

    def test_list_file(self):
        """List multiple files."""
        # Add two more files
        factories.CFileFactory()
        factories.CFileFactory()
        response = self._test_get(ENDPOINTS["files"], self.admin_user, 200)

        for cfile in models.CFile.objects.all():
            assert {
                "pk": cfile.pk,
                "file": "{}{}".format("http://fsociety.localhost", cfile.file.url),
            } in response.json()
