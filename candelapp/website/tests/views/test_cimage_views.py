"""CImage api tests."""
from django.db import connection

from candelapp.authentication.factories import AdminFactory, UserFactory
from candelapp.website import models, factories
from candelapp.tools import test_utils


ENDPOINTS = {
    "images": "/api/v1/website/images/",
}


class CImageAPITestCase(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    """CImage endpoint testcases."""

    @classmethod
    def setUpTestData(cls):
        """Set up users."""
        super().setUpTestData()
        cls.admin_user = AdminFactory()
        cls.non_admin_user = UserFactory()
        cls.cimage = factories.CImageFactory()

    def test_send_with_admin_user(self):
        """Try to get with a admin user."""
        self._test_get(
            "{}{}/".format(ENDPOINTS["images"], self.cimage.pk),
            self.admin_user,
            200,
        )

    def test_send_with_non_admin_user(self):
        """Try to get with a non admin user."""
        response = self._test_get(
            "{}{}/".format(ENDPOINTS["images"], self.cimage.pk),
            self.non_admin_user,
            403,
        )
        assert response.json() == {"detail": "You do not have permission to perform this action."}

    def test_not_found_image(self):
        """Try to get an unknown model."""
        self._test_get(
            "{}{}/".format(ENDPOINTS["images"], 293820938),
            self.admin_user,
            404,
        )

    def test_forbidden_methods(self):
        """Try to PATCH, DELETE images."""
        data = {"image": test_utils.get_gif()}
        self._test_patch(
            "{}{}/".format(ENDPOINTS["images"], self.cimage.pk),
            self.admin_user,
            405,
            data,
        )
        self._test_delete(
            "{}{}/".format(ENDPOINTS["images"], self.cimage.pk),
            self.admin_user,
            405,
        )

    def test_post_image(self):
        """Try to POST an image."""
        # A gif won't be optimized and converted to webp as it would lost animation
        data = {"image": test_utils.get_gif()}
        self._test_post(
            ENDPOINTS["images"],
            self.admin_user,
            201,
            data,
        )

        assert models.CImage.objects.count() == 2

        cimage = models.CImage.objects.last()
        assert "{}".format(connection.tenant.name) in cimage.image.url
        assert ".gif" in cimage.image.url

        # A png will be converted though
        data = {"image": test_utils.get_png()}
        self._test_post(
            ENDPOINTS["images"],
            self.admin_user,
            201,
            data,
        )

        assert models.CImage.objects.count() == 3

        cimage = models.CImage.objects.last()
        assert "{}".format(connection.tenant.name) in cimage.image.url
        assert ".webp" in cimage.image.url

    def test_list_image(self):
        """List multiple images."""
        # Add two more images
        factories.CImageFactory()
        factories.CImageFactory()
        response = self._test_get(ENDPOINTS["images"], self.admin_user, 200)

        for cimage in models.CImage.objects.all():
            assert {
                "pk": cimage.pk,
                "image": "{}{}".format("http://fsociety.localhost", cimage.image.url),
            } in response.json()
