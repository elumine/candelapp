"""Models api tests."""
from copy import copy

from candelapp.authentication.factories import AdminFactory, UserFactory
from candelapp.history.models import HistoryLog
from candelapp.website import factories, models, serializers
from candelapp.tools import test_utils


ENDPOINTS = {
    "models": "/api/v1/website/models/",
    "modelbyname": "/api/v1/website/models/get_by_name/",
    "available_models": "/api/v1/website/models/available_models_for_pages/",
}


class ModelAPITestCase(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    """ModelItem endpoint testcases."""

    @classmethod
    def setUpTestData(cls):
        """Set up users."""
        super().setUpTestData()
        cls.admin_user = AdminFactory()
        cls.non_admin_user = UserFactory()
        cls.model1 = factories.CModelFactory(verbose_name="model_test_1", type="multiple")
        cls.model2 = factories.CModelFactory(verbose_name="model_test_2", type="multiple")
        cls.model3_simple = factories.CModelFactory(verbose_name="model_simple_3", type="simple")

    def test_list_non_admin_user(self):
        # With model is_public to False
        assert self.model1.is_public is False

        # As a non admin user, It should return a 403
        self._test_get(
            ENDPOINTS["models"],
            self.non_admin_user,
            403,
        )

        # With model is_public to True
        self.model1.is_public = True
        self.model1.save()

        # It should not change
        self._test_get(
            ENDPOINTS["models"],
            self.non_admin_user,
            403,
        )

    def test_list_admin_user(self):
        response = self._test_get(
            ENDPOINTS["models"],
            self.admin_user,
            200,
        )
        assert len(response.json()) == 3

    def test_unsafe_methods_non_admin_user(self):
        # With model is_public to False
        assert self.model1.is_public is False

        # Post, patch, delete  as a non admin user should return a 403
        self._test_post(ENDPOINTS["models"], self.non_admin_user, 403, {}, as_json=True)

        self._test_patch(
            "{}{}/".format(ENDPOINTS["models"], self.model1.pk),
            self.non_admin_user,
            403,
            {},
            as_json=True,
            compare=False,
        )

        # Put as a non admin user, It should return a 403
        self._test_delete(
            "{}{}/".format(ENDPOINTS["models"], self.model1.pk),
            self.non_admin_user,
            403,
        )

        # With model is_public to True
        self.model1.is_public = True
        self.model1.save()

        # Post, patch, delete  as a non admin user should still return a 403
        self._test_post(ENDPOINTS["models"], self.non_admin_user, 403, {}, as_json=True)

        self._test_patch(
            "{}{}/".format(ENDPOINTS["models"], self.model1.pk),
            self.non_admin_user,
            403,
            {},
            as_json=True,
            compare=False,
        )

        self._test_delete(
            "{}{}/".format(ENDPOINTS["models"], self.model1.pk),
            self.non_admin_user,
            403,
        )

    def test_post_admin_user(self):
        self._test_post(
            ENDPOINTS["models"],
            self.admin_user,
            400,
        )

    def test_get_model_non_admin_user(self):
        # With model is_public to False
        assert self.model1.is_public is False

        # As a non admin user, It should return a 403
        self._test_get(
            "{}{}/".format(ENDPOINTS["models"], self.model1.pk),
            self.non_admin_user,
            403,
        )

        # With model is_public to True
        self.model1.is_public = True
        self.model1.save()

        # As a non admin user, it should return the model
        self._test_get(
            "{}{}/".format(ENDPOINTS["models"], self.model1.pk),
            self.non_admin_user,
            200,
        )

    def test_get_model_by_name_as_non_admin_user(self):
        # With model is_public to False
        assert self.model1.is_public is False

        # As a non admin user, It should return a 403
        self._test_get(
            "{}{}/".format(ENDPOINTS["modelbyname"], self.model1.name),
            self.non_admin_user,
            403,
        )

        # With model is_public to True
        self.model1.is_public = True
        self.model1.save()

        # As a non admin user, it should return the model
        self._test_get(
            "{}{}/".format(ENDPOINTS["modelbyname"], self.model1.name),
            self.non_admin_user,
            200,
        )

    def test_get_model_admin_user(self):
        response = self._test_get(
            "{}{}/".format(ENDPOINTS["models"], self.model1.pk),
            self.admin_user,
            200,
        )
        assert response.json() == serializers.CModelSerializer(self.model1).data

    def test_get_model_by_name_admin_user(self):
        """Try to get model by its name."""
        response = self._test_get(
            "{}{}/".format(ENDPOINTS["modelbyname"], self.model1.name),
            self.admin_user,
            200,
        )
        assert response.json() == serializers.CModelSerializer(self.model1).data

    def test_get_available_models(self):
        """Test to get available models for pages."""
        # The two models are available
        response = self._test_get(
            ENDPOINTS["available_models"],
            self.admin_user,
            200,
        )
        assert len(response.json()) == 2

        factories.CPageFactory(model=self.model1)

        response = self._test_get(
            ENDPOINTS["available_models"],
            self.admin_user,
            200,
        )
        assert len(response.json()) == 1
        assert response.json()[0]["name"] == "model_test_2"

        factories.CPageFactory(model=self.model2)

        response = self._test_get(
            ENDPOINTS["available_models"],
            self.admin_user,
            200,
        )
        assert len(response.json()) == 0

    def test_create_model(self):
        """Try to create a model."""
        # Empty payload
        self._test_post(ENDPOINTS["models"], self.admin_user, 400, {}, as_json=True)

        # Wrong payload
        data = {"hey": "hello"}
        self._test_post(ENDPOINTS["models"], self.admin_user, 400, data, as_json=True)

        fields = copy(test_utils.initial_fields)
        fields.append({"verbose_name": "Field 1", "required": True, "type": "text"})
        data = {
            "verbose_name": "Model",
            "fields": fields,
        }
        self._test_post(ENDPOINTS["models"], self.admin_user, 201, data, as_json=True)

        assert models.CModel.objects.count() == 4

        assert HistoryLog.objects.count() == 1
        hl = HistoryLog.objects.first()
        assert (
            hl.translated_action
            == f"Model {data['verbose_name']} has been created by {self.admin_user.email}."
        )
        assert hl.type == "model_add"
        assert hl.prev_state is None
        assert hl.new_state["verbose_name"] == data["verbose_name"]

    def test_update_model(self):
        """Try to update a model."""
        model = factories.CModelFactory(
            name="random_model",
            verbose_name="Random model",
        )

        # Name can't be edited
        data = {"verbose_name": "Hello how low"}

        response = self._test_patch(
            "{}{}/".format(ENDPOINTS["models"], model.pk),
            self.admin_user,
            400,
            data,
            as_json=True,
            compare=False,
        )
        assert response.json() == {"verbose_name": ["The model name can't be updated."]}

        # Type can't be edited
        data = {"type": "multiple"}

        response = self._test_patch(
            "{}{}/".format(ENDPOINTS["models"], model.pk),
            self.admin_user,
            400,
            data,
            as_json=True,
            compare=False,
        )
        assert response.json() == {"type": ["The model type can't be updated."]}

        # Patch fields
        fields = copy(test_utils.initial_fields)
        fields.append(
            {"verbose_name": "Field 1", "required": True, "type": "text"},
        )

        data = {"fields": fields}

        response = self._test_patch(
            "{}{}/".format(ENDPOINTS["models"], model.pk),
            self.admin_user,
            200,
            data,
            as_json=True,
            compare=False,
        )

        model.refresh_from_db()
        assert {
            "internal_name": "field_1",
            "verbose_name": "Field 1",
            "required": True,
            "type": "text",
        } in model.fields

        assert HistoryLog.objects.count() == 1
        hl = HistoryLog.objects.first()
        assert (
            hl.translated_action
            == f"Model {model.verbose_name} has been modified by {self.admin_user.email}."
        )
        assert hl.type == "model_change"
        assert hl.prev_state is not None
        assert hl.new_state != hl.prev_state

        # Patch category
        category = factories.CModelCategoryFactory()
        data = {"category": category.pk}
        response = self._test_patch(
            "{}{}/".format(ENDPOINTS["models"], model.pk),
            self.admin_user,
            200,
            data,
            as_json=True,
            compare=False,
        )

        model.refresh_from_db()
        assert model.category == category

    def test_delete(self):
        """Test to delete models."""
        cm1 = factories.CModelFactory(type="multiple")
        cm2 = factories.CModelFactory(type="multiple")

        assert models.CModel.objects.count() == 5

        # Try to delete a model without any modelitem or page
        self._test_delete(
            "{}{}/".format(ENDPOINTS["models"], cm1.pk),
            self.admin_user,
            204,
        )

        assert models.CModel.objects.count() == 4

        assert HistoryLog.objects.count() == 1
        hl = HistoryLog.objects.first()
        assert (
            hl.translated_action
            == f"Model {cm1.verbose_name} has been deleted by {self.admin_user.email}."
        )
        assert hl.type == "model_delete"
        assert hl.prev_state is not None
        assert hl.new_state is None

        # Add a modelitem and a page
        modelitem = factories.CModelItemFactory(model=cm2)
        page = factories.CPageFactory(model=cm2)

        # A modelitem exists and stops the deletion
        response = self._test_delete(
            "{}{}/".format(ENDPOINTS["models"], cm2.pk),
            self.admin_user,
            400,
        )
        assert response.json() == {
            "non_field_errors": (
                "The model cannot be deleted because " "it has objects created with it."
            )
        }
        modelitem.delete()

        # A page exists
        response = self._test_delete(
            "{}{}/".format(ENDPOINTS["models"], cm2.pk),
            self.admin_user,
            400,
        )
        assert response.json() == {
            "non_field_errors": (
                "The template cannot be deleted because " "it is associated with a page."
            )
        }
        # Nothing should stop the deletion now
        page.delete()

        self._test_delete(
            "{}{}/".format(ENDPOINTS["models"], cm2.pk),
            self.admin_user,
            204,
        )

        assert models.CModel.objects.count() == 3

        # A simple model with modelitems will delete them all at deletion
        cm3 = factories.CModelFactory(type="simple")
        cmi = factories.CModelItemFactory(model=cm3)

        self._test_delete(
            "{}{}/".format(ENDPOINTS["models"], cm3.pk),
            self.admin_user,
            204,
        )

        with self.assertRaises(models.CModel.DoesNotExist):
            cm3.refresh_from_db()
        with self.assertRaises(models.CModelItem.DoesNotExist):
            cmi.refresh_from_db()
