from candelapp.authentication.factories import UserFactory
from candelapp.groups.factories import GroupFactory
from candelapp.international.models import Language
from candelapp.settings.models import Settings
from candelapp.tools import test_utils
from candelapp.website import factories


ENDPOINTS = {
    "modelitem": "/api/v1/website/modelitems/",
    "modelitem-parents": "/api/v1/website/modelitems/{}/get_parents/",
    "modelitem-child": "/api/v1/website/modelitems/{}/{}/get_child/{}/",
    "modelitem-lang": "/api/v1/website/modelitems/{}/lang/{}/",
}


class ModelItemPermissionsTestCase(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    @classmethod
    def setUpTestData(cls):
        """Set up users."""
        super().setUpTestData()
        settings = Settings.objects.first()
        cls.cv_lang = Language.objects.get(code="cv")
        settings.secondary_languages.add(cls.cv_lang)
        settings.save()

        cls.user1 = UserFactory()
        cls.user2 = UserFactory()

        cls.group1 = GroupFactory(users=[cls.user1])
        cls.group2 = GroupFactory(users=[cls.user2])
        cls.group12 = GroupFactory(users=[cls.user1, cls.user2])

        cls.model1 = factories.CModelFactory(type="multiple")
        cls.modelitem1 = factories.CModelItemFactory(model=cls.model1)
        cls.modelitem2 = factories.CModelItemFactory(model=cls.model1)
        cls.modelitem3 = factories.CModelItemFactory(model=cls.model1)
        cls.child_modelitem1 = factories.CModelItemFactory(
            model=cls.model1, parent=cls.modelitem1, lang=cls.cv_lang
        )

        cls.model2 = factories.CModelFactory(type="multiple")
        cls.modelitem4 = factories.CModelItemFactory(model=cls.model2)
        cls.modelitem5 = factories.CModelItemFactory(model=cls.model2)

    def test_post_anonymous_no_perm(self):
        # With no permissions and an anonymous user
        # It should return a 401
        self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            None,
            401,
            {},
            as_json=True,
        )

    def test_post_anonymous_with_allow_everyone(self):
        # With 'allow_everyone' in 'can_add' permission and an anonymous user
        self.model1.can_add = {"groups": [], "allow_everyone": True}
        self.model1.save()
        # It should return a 400 meaning we have access to it
        self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            None,
            400,
            {},
            as_json=True,
        )

        # It should work too by using the model name instead of the pk
        self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.name),
            None,
            400,
            {},
            as_json=True,
        )

    def test_post_unknown_item(self):
        self.model1.can_add = {"groups": [], "allow_everyone": True}
        self.model1.save()

        # As an authorized anonymous
        self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], 2983),
            None,
            404,
            {},
            as_json=True,
        )

        # Authenticated user
        self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], 2983),
            self.user1,
            404,
            {},
            as_json=True,
        )

    def test_post_authenticated_no_perm(self):
        # With no permissions and an authenticated user
        # It should return a 403
        self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            self.user1,
            403,
            {},
            as_json=True,
        )

    def test_post_auth_with_allow_everyone(self):
        # With 'allow_everyone' in 'can_add' permission and an authenticated user
        self.model1.can_add = {"groups": [], "allow_everyone": True}
        self.model1.save()
        # It should return a 400 meaning we have access to it
        self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            self.user1,
            400,
            {},
            as_json=True,
        )

        # It should work too by using the model name instead of the pk
        self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.name),
            self.user1,
            400,
            {},
            as_json=True,
        )

    def test_post_with_groups_own_objects(self):
        # With 'groups_own_object' and an authenticated user
        self.model1.can_add = {"groups": [], "groups_own_objects": True}
        self.model1.save()

        # It should not return a 400
        self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            self.user1,
            400,
            {},
            as_json=True,
        )

        # With another authenticated user, it should return a 400 too
        self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            self.user2,
            400,
            {},
            as_json=True,
        )

        # An anonymous user would be rejected
        self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            False,
            401,
            {},
            as_json=True,
        )

    def test_post_with_allowed_group(self):
        # With no 'groups_own_object' but allowed group and an authenticated user
        self.model1.can_add = {"groups": [self.group1.pk]}
        self.model1.save()
        # It should not return a 400
        self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            self.user1,
            400,
            {},
            as_json=True,
        )

        # But as user2 is not in the allowed group, it should raise a 403 for him
        self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            self.user2,
            403,
            {},
            as_json=True,
        )

        # An anonymous user would be rejected
        self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            None,
            401,
            {},
            as_json=True,
        )

    def test_patch_anonymous_no_perm(self):
        # With no permissions and an anonymous user
        # It should return a 401
        self._test_patch(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            None,
            401,
            {},
            as_json=True,
        )

    def test_patch_anonymous_with_allow_everyone(self):
        # With 'allow_everyone' in 'can_edit' and an anonymous user
        self.model1.can_edit = {"allow_everyone": True}
        self.model1.save()
        # It should return a 400
        self._test_patch(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            None,
            400,
            {},
            as_json=True,
        )

    def test_patch_authenticated_no_perm(self):
        # With no permissions and an authenticated user
        # It should return a 403
        self._test_patch(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            self.user1,
            403,
            {},
            as_json=True,
        )

    def test_patch_auth_with_allow_everyone(self):
        # With 'allow_everyone' in 'can_add' permission and an authenticated user
        self.model1.can_edit = {"groups": [], "allow_everyone": True}
        self.model1.save()
        # It should return a 400 meaning we have access to it
        self._test_patch(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            self.user1,
            400,
            {},
            as_json=True,
        )

    def test_patch_with_groups_own_objects(self):
        # With 'groups_own_object' and an authenticated user
        self.model1.can_edit = {"groups": [], "groups_own_objects": True}
        self.model1.save()

        # It should return a 403 as the modelitem has no group
        self._test_patch(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            self.user1,
            403,
            {},
            as_json=True,
        )

        self.modelitem1.groups.add(self.group1)
        self.modelitem1.save()

        # It should work now
        self._test_patch(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            self.user1,
            400,
            {},
            as_json=True,
        )

        # But not for another user
        self._test_patch(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            self.user2,
            403,
            {},
            as_json=True,
        )

        # Even less for an anonymous
        self._test_patch(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            None,
            401,
            {},
            as_json=True,
        )

    def test_patch_with_allowed_groups(self):
        # With groups and an authenticated users
        self.model1.can_edit = {"groups": [self.group2.pk]}
        self.model1.save()

        # We don't needgroup on modelitem here
        assert self.modelitem1.groups.count() == 0

        # It should return a 403 as the user groups are not in the perm groups
        self._test_patch(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            self.user1,
            403,
            {},
            as_json=True,
        )

        # But the other user is
        self._test_patch(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            self.user2,
            400,
            {},
            as_json=True,
        )

        # Not for an anonymous
        self._test_patch(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            None,
            401,
            {},
            as_json=True,
        )

    def test_detail_anonymous_no_perm(self):
        # With no permissions and an anonymous user
        # It should return a 401
        self._test_get(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            None,
            401,
        )

    def test_detail_anonymous_with_allow_everyone(self):
        # With 'allow_everyone' in 'can_view' and an anonymous user
        self.model1.can_view = {"allow_everyone": True}
        self.model1.save()
        # It should return a 200
        self._test_get(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            None,
            200,
        )

    def test_detail_authenticated_no_perm(self):
        # With no permissions and an authenticated user
        # It should return a 403
        self._test_get(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            self.user1,
            403,
        )

    def test_detail_with_allow_everyone(self):
        # With 'groups_own_object' and an authenticated user
        self.model1.can_view = {"groups": [], "allow_everyone": True}
        self.model1.save()

        # It should return a 403 as the modelitem has no group
        self._test_get(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            self.user1,
            200,
        )

    def test_detail_with_groups_own_objects(self):
        # With 'groups_own_object' and an authenticated user
        self.model1.can_view = {"groups": [], "groups_own_objects": True}
        self.model1.save()

        # It should return a 403 as the modelitem has no group
        self._test_get(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            self.user1,
            403,
        )

        self.modelitem1.groups.add(self.group1)
        self.modelitem1.save()

        # It should work now
        self._test_get(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            self.user1,
            200,
        )

        # But not for another user
        self._test_get(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            self.user2,
            403,
        )

        # Even less for an anonymous
        self._test_get(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            None,
            401,
        )

    def test_detail_with_allowed_groups(self):
        # With groups and an authenticated users
        self.model1.can_view = {"groups": [self.group2.pk]}
        self.model1.save()

        # We don't needgroup on modelitem here
        assert self.modelitem1.groups.count() == 0

        # It should return a 403 as the user groups are not in the perm groups
        self._test_get(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            self.user1,
            403,
        )

        # But the other user is
        self._test_get(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            self.user2,
            200,
        )

        # Not for an anonymous
        self._test_get(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            None,
            401,
        )

    def test_delete_anonymous_no_perm(self):
        # With no permissions and an anonymous user
        # It should return a 401
        self._test_delete(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            None,
            401,
        )

    def test_delete_anonymous_with_allow_everyone(self):
        # With 'allow_everyone' in 'can_delete' and an anonymous user
        self.model1.can_delete = {"allow_everyone": True}
        self.model1.save()
        # It should return a 204
        self._test_delete(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            None,
            204,
        )

    def test_delete_authenticated_no_perm(self):
        # With no permissions and an authenticated user
        # It should return a 403
        self._test_delete(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            self.user1,
            403,
        )

    def test_delete_with_allow_everyone(self):
        # With 'allow_everyone' and an authenticated user
        self.model1.can_delete = {"groups": [], "allow_everyone": True}
        self.model1.save()

        # It should return a 403 as the modelitem has no group
        self._test_delete(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            self.user1,
            204,
        )

    def test_delete_with_groups_own_objects(self):
        # With 'groups_own_object' and an authenticated user
        self.model1.can_delete = {"groups": [], "groups_own_objects": True}
        self.model1.save()

        # It should return a 403 as the modelitem has no group
        self._test_delete(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            self.user1,
            403,
        )

        self.modelitem1.groups.add(self.group1)
        self.modelitem1.save()

        # It should work now
        self._test_delete(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            self.user1,
            204,
        )

        # But not for another user
        self._test_delete(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            self.user2,
            404,
        )

        # Even less for an anonymous
        self._test_delete(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            None,
            404,
        )

    def test_delete_with_groups_own_objects_not_authorized(self):
        # With 'groups_own_object' and an authenticated user
        self.model1.can_delete = {"groups": [], "groups_own_objects": True}
        self.model1.save()

        # It should return a 403 as the modelitem has no group
        self._test_delete(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            self.user1,
            403,
        )

        self.modelitem1.groups.add(self.group1)
        self.modelitem1.save()

        # But not for another user
        self._test_delete(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            self.user2,
            403,
        )

        # Even less for an anonymous
        self._test_delete(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            None,
            401,
        )

    def test_delete_with_allowed_groups(self):
        # With groups and an authenticated users
        self.model1.can_delete = {"groups": [self.group2.pk]}
        self.model1.save()

        # We don't needgroup on modelitem here
        assert self.modelitem1.groups.count() == 0

        # It should return a 403 as the user groups are not in the perm groups
        self._test_delete(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            self.user1,
            403,
        )

        # But the other user is
        self._test_delete(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            self.user2,
            204,
        )

        # Not for an anonymous
        self._test_delete(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.pk, self.modelitem1.pk),
            None,
            404,
        )

    def test_list_modelitems_groups_own_objects(self):
        # With no permissions
        self._test_get(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            self.user1,
            403,
        )

        self._test_get(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            self.user2,
            403,
        )

        self._test_get(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            None,
            401,
        )

        # With 'groups_own_objects' the the modelitem assigned to the same group
        # of the user
        self.model1.can_view = {"groups_own_objects": True}
        self.model1.save()
        self.modelitem1.groups.add(self.group1)
        self.modelitem1.save()
        response = self._test_get(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            self.user1,
            200,
        )
        assert len(response.json()) == 1

        # A user from another group won't access to it
        response = self._test_get(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            self.user2,
            200,
        )
        assert len(response.json()) == 0

        # Anon would be asked to login
        self._test_get(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            None,
            401,
        )

        # Setting a second modelitem with the two groups
        self.modelitem2.groups.add(self.group1, self.group2)
        self.modelitem2.save()

        # The user1 will get the first modelitem and the second
        response = self._test_get(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            self.user1,
            200,
        )
        assert len(response.json()) == 2

        # And the other user will get only the second one
        response = self._test_get(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            self.user2,
            200,
        )
        assert len(response.json()) == 1

        # Anon would be asked to login
        self._test_get(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            None,
            401,
        )

        # By trying to get the child without having it in the group
        response = self._test_get(
            ENDPOINTS["modelitem-child"].format(
                self.model1.pk, self.modelitem1.pk, self.cv_lang.code
            ),
            self.user1,
            403,
        )

        response = self._test_get(
            ENDPOINTS["modelitem-child"].format(
                self.model1.pk, self.modelitem1.pk, self.cv_lang.code
            ),
            None,
            401,
        )

        # By setting a child in the first group
        self.child_modelitem1.groups.add(self.group1)
        self.child_modelitem1.save()

        # We should have 3 modelitems for user1
        response = self._test_get(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            self.user1,
            200,
        )
        assert len(response.json()) == 3

        # But still 2 parents
        response = self._test_get(
            ENDPOINTS["modelitem-parents"].format(self.model1.pk),
            self.user1,
            200,
        )
        assert len(response.json()) == 2

        # And the child
        response = self._test_get(
            ENDPOINTS["modelitem-child"].format(
                self.model1.pk, self.modelitem1.pk, self.cv_lang.code
            ),
            self.user1,
            200,
        )
        assert response.json()["pk"] == self.child_modelitem1.pk

        # Anon would still be rejected
        self._test_get(
            ENDPOINTS["modelitem-parents"].format(self.model1.pk),
            None,
            401,
        )

        self._test_get(
            ENDPOINTS["modelitem-child"].format(
                self.model1.pk, self.modelitem1.pk, self.cv_lang.code
            ),
            None,
            401,
        )

    def test_list_modelitems_allow_everyone(self):
        # With no permissions
        self._test_get(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            None,
            401,
        )

        self._test_get(
            ENDPOINTS["modelitem-parents"].format(self.model1.pk),
            None,
            401,
        )

        self._test_get(
            ENDPOINTS["modelitem-child"].format(
                self.model1.pk, self.modelitem1.pk, self.cv_lang.code
            ),
            None,
            401,
        )

        self._test_get(
            ENDPOINTS["modelitem-lang"].format(self.model1.pk, "cv"),
            None,
            401,
        )

        # With 'allow_everyone'
        self.model1.can_view = {"allow_everyone": True}
        self.model1.save()

        # There are 4 modelitems for model1
        response = self._test_get(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            None,
            200,
        )
        assert len(response.json()) == 4

        # 3 parents
        response = self._test_get(
            ENDPOINTS["modelitem-parents"].format(self.model1.pk),
            None,
            200,
        )
        assert len(response.json()) == 3

        # 1 child
        response = self._test_get(
            ENDPOINTS["modelitem-child"].format(
                self.model1.pk, self.modelitem1.pk, self.cv_lang.code
            ),
            None,
            200,
        )
        assert response.json()["pk"] == self.child_modelitem1.pk

        response = self._test_get(
            ENDPOINTS["modelitem-lang"].format(self.model1.pk, "cv"),
            None,
            200,
        )
        assert len(response.json()) == 1

        # By setting 'groups_own_object' for group2, it won't affect
        # what the anon user will get
        self.model2.can_view = {"groups_own_objects": True}
        self.model2.save()
        self.modelitem4.groups.add(self.group2)
        self.modelitem4.save()

        response = self._test_get(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            None,
            200,
        )
        assert len(response.json()) == 4

        # Nor with group permissions
        self.model2.can_view["groups"] = [self.group1.pk, self.group2.pk]
        response = self._test_get(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            None,
            200,
        )
        assert len(response.json()) == 4

    def test_list_modelitems_perm_groups(self):
        # With no permissions
        self._test_get(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            self.user1,
            403,
        )

        self._test_get(
            ENDPOINTS["modelitem-parents"].format(self.model1.pk),
            self.user1,
            403,
        )

        self._test_get(
            ENDPOINTS["modelitem-child"].format(
                self.model1.pk, self.modelitem1.pk, self.cv_lang.code
            ),
            self.user1,
            403,
        )

        # With group1 in group perms
        self.model1.can_view["groups"] = [self.group1.pk]
        self.model1.save()

        # We should have 4 modelitems (3 parents 1 child)
        response = self._test_get(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            self.user1,
            200,
        )
        assert len(response.json()) == 4

        # 3 parents
        response = self._test_get(
            ENDPOINTS["modelitem-parents"].format(self.model1.pk),
            self.user1,
            200,
        )
        assert len(response.json()) == 3

        # 1 child
        response = self._test_get(
            ENDPOINTS["modelitem-child"].format(
                self.model1.pk, self.modelitem1.pk, self.cv_lang.code
            ),
            self.user1,
            200,
        )
        assert response.json()["pk"] == self.child_modelitem1.pk

        # A user from another group won't get any of them
        self._test_get(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            self.user2,
            403,
        )

        self._test_get(
            ENDPOINTS["modelitem-parents"].format(self.model1.pk),
            self.user2,
            403,
        )

        self._test_get(
            ENDPOINTS["modelitem-child"].format(
                self.model1.pk, self.modelitem1.pk, self.cv_lang.code
            ),
            self.user2,
            403,
        )
