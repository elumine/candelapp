"""Modelitem api tests."""
from copy import copy

from candelapp.authentication.factories import AdminFactory, UserFactory
from candelapp.groups.factories import GroupFactory
from candelapp.history.models import HistoryLog
from candelapp.international.models import Language
from candelapp.settings.models import Settings
from candelapp.tools import test_utils
from candelapp.website import models, factories


ENDPOINTS = {
    "modelitem": "/api/v1/website/modelitems/",
    "modelitem-parents": "/api/v1/website/modelitems/{}/get_parents/",
    "modelitem-child": "/api/v1/website/modelitems/{}/{}/get_child/{}/",
    "modelitem-by-lang": "/api/v1/website/modelitems/{}/lang/{}/",
}


class ModelItemAPITestCase(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    """ModelItem endpoint testcases."""

    @classmethod
    def setUpTestData(cls):
        """Set up users."""
        super().setUpTestData()
        settings = Settings.objects.first()
        cls.primary_lang = settings.primary_language
        cls.cv_lang = Language.objects.get(code="cv")
        cls.es_lang = Language.objects.get(code="es")
        cls.fi_lang = Language.objects.get(code="fi")
        settings.secondary_languages.add(cls.cv_lang, cls.es_lang, cls.fi_lang)
        settings.save()

        cls.admin_user = AdminFactory()
        cls.non_admin_user = UserFactory()
        cls.model1 = factories.CModelFactory(type="multiple")
        cls.model2 = factories.CModelFactory(type="simple")

    def test_get_with_admin_user(self):
        """Try to get with a admin user."""
        self._test_get(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.name),
            self.admin_user,
            200,
        )

    def test_get_with_non_admin_user(self):
        """Try to get with a non admin user."""
        self._test_get(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.name),
            self.non_admin_user,
            403,
        )

    def test_post_with_non_admin_user(self):
        """Try to post with a non admin user."""
        self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.name),
            self.non_admin_user,
            403,
        )

    def test_post_with_admin_user(self):
        """Try to post with a admin user."""
        self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.name),
            self.admin_user,
            400,
        )

    def test_unknown_model(self):
        """Try to get an unknown model."""
        self._test_get(
            "{}{}/".format(ENDPOINTS["modelitem"], "unknown_model"),
            self.admin_user,
            404,
        )
        data = {"data": {"hey": "yo"}}
        self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], "unknown_model"),
            self.admin_user,
            404,
            data,
            as_json=True,
        )

    def test_list_modelitems(self):
        """List modelitems."""
        # Create two model items with model1
        factories.CModelItemFactory(model=self.model1)
        factories.CModelItemFactory(model=self.model1)

        # Create one model item with a new model
        cmi3 = factories.CModelItemFactory()

        # Model1 should have two model items
        response = self._test_get(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.name),
            self.admin_user,
            200,
        )
        assert len(response.json()) == 2

        # List with pk works too
        response = self._test_get(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.pk),
            self.admin_user,
            200,
        )
        assert len(response.json()) == 2

        # Model2 should have only one
        response = self._test_get(
            "{}{}/".format(ENDPOINTS["modelitem"], cmi3.model.name),
            self.admin_user,
            200,
        )
        assert len(response.json()) == 1

    def test_get_modelitem(self):
        """Test get modelitem."""
        self._test_get(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.name, 92398),
            self.admin_user,
            404,
        )

        cmi = factories.CModelItemFactory()
        self._test_get(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], cmi.model.name, cmi.pk),
            self.admin_user,
            200,
        )

        # You can get modelitems with model name too
        self._test_get(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], cmi.model.pk, cmi.pk),
            self.admin_user,
            200,
        )

    def test_get_children(self):
        """Get get modelitem's children."""
        parent = factories.CModelItemFactory(model=self.model1)
        factories.CModelItemFactory(parent=parent, model=self.model1, lang=self.fi_lang)

        # Parent not existing
        self._test_get(
            ENDPOINTS["modelitem-child"].format(self.model1.name, 92398, self.fi_lang.code),
            self.admin_user,
            404,
        )

        # Lang not existing
        self._test_get(
            ENDPOINTS["modelitem-child"].format(self.model1.name, parent.pk, "xx"),
            self.admin_user,
            404,
        )

        # Something is wrong with the url
        self._test_get(
            ENDPOINTS["modelitem-child"].format(2.3, "undefined", 2),
            self.admin_user,
            404,
        )

        # Child could exist but not in this language yet
        self._test_get(
            ENDPOINTS["modelitem-child"].format(self.model1.name, parent.pk, self.es_lang.code),
            self.admin_user,
            204,
        )

        self._test_get(
            ENDPOINTS["modelitem-child"].format(self.model1.name, parent.pk, self.fi_lang.code),
            self.admin_user,
            200,
        )

    def test_get_parents(self):
        """Test get parents."""
        # One parent one child
        parent = factories.CModelItemFactory(model=self.model1)
        factories.CModelItemFactory(model=self.model1, parent=parent, lang=self.fi_lang)

        # Two parents one child
        model2 = factories.CModelFactory(type="multiple")
        parent2 = factories.CModelItemFactory(model=model2)
        parent3 = factories.CModelItemFactory(model=model2)
        factories.CModelItemFactory(model=model2, parent=parent2, lang=self.fi_lang)

        response = self._test_get(
            ENDPOINTS["modelitem-parents"].format(self.model1.name),
            self.admin_user,
            200,
        )
        assert len(response.json()) == 1
        assert response.json()[0]["pk"] == parent.pk

        response = self._test_get(
            ENDPOINTS["modelitem-parents"].format(parent2.model.name),
            self.admin_user,
            200,
        )
        assert len(response.json()) == 2
        assert response.json()[0]["pk"] == parent2.pk
        assert response.json()[1]["pk"] == parent3.pk

    def test_get_modelitems_by_lang(self):
        """Get get modelitems from a model by lang."""
        parent = factories.CModelItemFactory(model=self.model1)
        parent2 = factories.CModelItemFactory(model=self.model1)
        parent3 = factories.CModelItemFactory(model=self.model2)
        item1 = factories.CModelItemFactory(parent=parent, model=self.model1, lang=self.fi_lang)
        item2 = factories.CModelItemFactory(parent=parent2, model=self.model1, lang=self.fi_lang)
        item3 = factories.CModelItemFactory(parent=parent2, model=self.model2, lang=self.es_lang)

        # No french items
        response = self._test_get(
            ENDPOINTS["modelitem-by-lang"].format(self.model1.name, "fr"),
            self.admin_user,
            200,
        )
        assert len(response.json()) == 0

        # Two english items with model1
        response = self._test_get(
            ENDPOINTS["modelitem-by-lang"].format(self.model1.name, "en"),
            self.admin_user,
            200,
        )
        assert len(response.json()) == 2
        assert response.json()[0]["pk"] == parent.pk
        assert response.json()[1]["pk"] == parent2.pk

        # Two fins items with model1
        response = self._test_get(
            ENDPOINTS["modelitem-by-lang"].format(self.model1.name, "fi"),
            self.admin_user,
            200,
        )
        assert len(response.json()) == 2
        assert response.json()[0]["pk"] == item1.pk
        assert response.json()[1]["pk"] == item2.pk

        # One english item with model2
        response = self._test_get(
            ENDPOINTS["modelitem-by-lang"].format(self.model2.pk, "en"),
            self.admin_user,
            200,
        )
        assert len(response.json()) == 1
        assert response.json()[0]["pk"] == parent3.pk

        # One spanish item with model2
        response = self._test_get(
            ENDPOINTS["modelitem-by-lang"].format(self.model2.pk, "es"),
            self.admin_user,
            200,
        )
        assert len(response.json()) == 1
        assert response.json()[0]["pk"] == item3.pk

    def test_create_modelitem(self):
        """Try to post model items."""
        # Empty data
        # It obviously is missing every serializer fields
        data = {}
        response = self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.name),
            self.admin_user,
            400,
            data,
            as_json=True,
        )
        assert response.json() == {
            "data": ["This field is required."],
            "lang": ["This field is required."],
        }

        # Missing title and description
        data = {"lang": self.primary_lang.pk, "data": {"hey": "yo"}}
        response = self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.name),
            self.admin_user,
            400,
            data,
            as_json=True,
        )
        assert response.json() == {
            "title": ["This field is required."],
        }

        # Unknown field
        data = {
            "lang": self.primary_lang.pk,
            "data": {
                "title": "title of the object",
                "description": "description",
                "ahoy": "m8",
            },
        }
        response = self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.name),
            self.admin_user,
            400,
            data,
            as_json=True,
        )
        assert response.json() == {"ahoy": ["This field is not known."]}

        # Parent page should be primary lang
        data = {
            "lang": self.cv_lang.pk,
            "data": {},
        }
        response = self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.name),
            self.admin_user,
            400,
            data,
            as_json=True,
        )
        assert response.json() == {"lang": ["The language of a parent must be the main language."]}

        # Only one parent with a simple model
        parent = factories.CModelItemFactory(parent=None, model=self.model2)
        data = {
            "lang": self.primary_lang.pk,
            "parent": None,
            "data": {},
            "model": self.model2.pk,
        }
        response = self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], parent.model.name),
            self.admin_user,
            400,
            data,
            as_json=True,
        )
        assert response.json() == {
            "non_field_errors": ["Only one parent can be created with a simple model."]
        }

        parent = factories.CModelItemFactory(lang=self.primary_lang)
        # Child page can't be set with primary lang
        data = {
            "lang": self.primary_lang.pk,
            "parent": parent.pk,
            "data": {},
        }
        response = self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], parent.model.name),
            self.admin_user,
            400,
            data,
            as_json=True,
        )
        assert response.json() == {
            "lang": [
                "The language of a child cannot be used by another "
                "child or be the main language."
            ]
        }
        # A child can't have an already used language
        factories.CModelItemFactory(lang=self.cv_lang, parent=parent, model=parent.model)
        data = {
            "lang": self.cv_lang.pk,
            "parent": parent.pk,
            "data": {},
        }
        response = self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], parent.model.name),
            self.admin_user,
            400,
            data,
            as_json=True,
        )
        assert response.json() == {
            "lang": [
                "The language of a child cannot be used by another "
                "child or be the main language."
            ]
        }
        #  Ok
        data = {
            "lang": self.primary_lang.pk,
            "data": {
                "title": "name of the object",
                "description": "description",
                "written": "1993-12-12",
                "price": 3.4,
                "content": "Hello how low",
                "picture": "https://candelapp.com/gif.gif",
            },
        }
        self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.name),
            self.admin_user,
            201,
            data,
            as_json=True,
        )

        models.CModelItem.objects.count() == 1
        cmi = models.CModelItem.objects.last()

        # Compare CModelItem items with data ones
        set(cmi.data.items()) - set(data["data"].items()) == set()

        assert HistoryLog.objects.count() == 1
        hl = HistoryLog.objects.first()
        assert hl.translated_action == (
            f"Item {cmi.data['title']} has been created by {self.admin_user.email}."
        )
        assert hl.type == "modelitem_add"
        assert hl.prev_state is None
        assert hl.new_state["data"]["title"] == cmi.data["title"]

    def test_create_with_status(self):
        # As an admin, I can create modelitem with any status
        data = {
            "lang": self.primary_lang.pk,
            "status": "active",
            "data": {
                "title": "name of the object",
                "description": "description",
                "written": "1993-12-12",
                "price": 3.4,
                "content": "Hello how low",
                "picture": "https://candelapp.com/gif.gif",
            },
        }
        self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.name),
            self.admin_user,
            201,
            data,
            as_json=True,
        )

        # As a user with the correct permissions
        group = GroupFactory()
        self.non_admin_user.groups.add(group)
        self.non_admin_user.save()
        self.model1.can_add = {"groups": [group.pk]}
        self.model1.save()

        # It should raise an error as only draft status can be used by a user
        response = self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.name),
            self.non_admin_user,
            400,
            data,
            as_json=True,
        )
        assert response.json() == {
            "status": ["You do not have permission to use any other status than draft."]
        }

        # But with status draft
        data["status"] = "draft"
        # It should work
        response = self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model1.name),
            self.non_admin_user,
            201,
            data,
            as_json=True,
        )
        assert response.json()["status"] == "draft"

    def test_patch(self):
        """Test to patch model items."""
        cmi = factories.CModelItemFactory(model=self.model1)

        data = {"data": cmi.data}
        data["data"]["title"] = "New name"
        self._test_patch(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.name, cmi.pk),
            self.admin_user,
            200,
            data,
            as_json=True,
            compare=False,
        )
        cmi.refresh_from_db()
        assert cmi.data["title"] == "New name"

        assert HistoryLog.objects.count() == 1
        hl = HistoryLog.objects.last()
        assert hl.translated_action == (
            f"Item {cmi.data['title']} has been modified by {self.admin_user.email}."
        )
        assert hl.type == "modelitem_change"
        assert hl.prev_state["data"]["title"] != "New name"
        assert hl.new_state["data"]["title"] == "New name"

        data["data"]["unknown_field"] = "Unknown"
        response = self._test_patch(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.name, cmi.pk),
            self.admin_user,
            400,
            data,
            as_json=True,
        )
        assert response.json() == {"unknown_field": ["This field is not known."]}

        parent = factories.CModelItemFactory(model=self.model1)
        data["data"].pop("unknown_field")

        # Try to update parent or lang won't affect modelitem
        data = {"lang": self.cv_lang.pk, "parent": parent.pk, "data": cmi.data}
        response = self._test_patch(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.name, cmi.pk),
            self.admin_user,
            200,
            data,
            as_json=True,
            compare=False,
        )
        assert response.json()["parent"] != parent.pk
        assert response.json()["lang"] != self.cv_lang.pk

    def test_delete(self):
        """Test to delete items."""
        parent = factories.CModelItemFactory(model=self.model1)
        parent2 = factories.CModelItemFactory(model=self.model1)
        cmi = factories.CModelItemFactory(model=self.model1, parent=parent2, lang=self.fi_lang)

        assert models.CModelItem.objects.count() == 3

        # A parent with no child
        self._test_delete(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.name, parent.pk),
            self.admin_user,
            204,
        )
        assert models.CModelItem.objects.count() == 2

        assert HistoryLog.objects.count() == 1
        hl = HistoryLog.objects.last()
        assert hl.translated_action == (
            f"Item {parent.data['title']} has been deleted by {self.admin_user.email}."
        )
        assert hl.type == "modelitem_delete"
        assert hl.prev_state["data"]["title"] == parent.data["title"]
        assert hl.new_state is None

        # A child
        self._test_delete(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.name, cmi.pk),
            self.admin_user,
            403,
        )
        assert models.CModelItem.objects.count() == 2

        # Parent with a child
        self._test_delete(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model1.name, parent2.pk),
            self.admin_user,
            204,
        )

        assert models.CModelItem.objects.count() == 0

    def test_required_fields(self):
        """Fields from models can be required if needed."""
        fields = copy(test_utils.initial_fields)
        fields.append(
            {
                "internal_name": "written",
                "type": "text",
                "verbose_name": "Written",
                "required": True,
            }
        )
        fields.append(
            {
                "internal_name": "picture",
                "type": "image",
                "verbose_name": "Picture",
                "required": True,
            }
        )
        model = factories.CModelFactory(fields=fields)
        data = {
            "lang": self.primary_lang.pk,
            "data": {"title": "Hello", "description": "Desc"},
        }
        response = self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], model.name),
            self.admin_user,
            400,
            data,
            as_json=True,
        )
        assert response.json() == {
            "picture": ["This field is required."],
            "written": ["This field is required."],
        }


class ModelItemAPISortTestCase(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    """ModelItem endpoint testcases using sorting."""

    @classmethod
    def setUpTestData(cls):
        """Set up objects and users."""
        super().setUpTestData()
        settings = Settings.objects.first()
        cls.primary_lang = settings.primary_language
        cls.cv_lang = Language.objects.get(code="cv")
        settings.secondary_languages.add(cls.cv_lang)

        cls.admin_user = AdminFactory()

        model_fields = copy(test_utils.initial_fields)
        model_fields.append(
            {
                "type": "number",
                "extras": [{}],
                "required": True,
                "verbose_name": "price",
                "internal_name": "price",
            }
        )
        model_fields.append(
            {
                "type": "text",
                "extras": [{}],
                "required": True,
                "verbose_name": "text",
                "internal_name": "text",
            }
        )
        model_fields.append(
            {
                "type": "choice",
                "extras": [{}],
                "choices": ["Yes", "No"],
                "required": True,
                "verbose_name": "choice",
                "internal_name": "choice",
            }
        )

        cls.model1 = factories.CModelFactory(type="multiple", fields=model_fields)
        cls.model2 = factories.CModelFactory(type="multiple", fields=model_fields)
        cls.modelitem1 = factories.CModelItemFactory(
            model=cls.model1,
            data={"title": "Hey1", "price": 1, "text": "azerty", "choice": "Yes"},
        )

        cls.modelitem2 = factories.CModelItemFactory(
            model=cls.model1,
            data={"title": "Hey2", "price": 2, "text": "bepo", "choice": "No"},
        )
        cls.modelitem3 = factories.CModelItemFactory(
            model=cls.model1,
            data={"title": "Hey3", "price": 3, "text": "qwerty", "choice": "Yes"},
        )

        cls.modelitem4 = factories.CModelItemFactory(
            model=cls.model2,
            data={"title": "Hey4", "price": 4, "text": "bye", "choice": "No"},
        )
        cls.modelitem5 = factories.CModelItemFactory(
            model=cls.model2,
            data={"title": "Hey5", "price": 5, "text": "bye", "choice": "Yes"},
        )

    def test_ordering_by_date(self):
        # By sorting from oldest to newest
        # We should get the 3 items from model1
        response = self._test_get(
            "{}{}/?ordering=created".format(ENDPOINTS["modelitem"], self.model1.name),
            self.admin_user,
            200,
        )
        assert len(response.data) == 3
        assert response.data[0]["pk"] == self.modelitem1.pk
        assert response.data[1]["pk"] == self.modelitem2.pk
        assert response.data[2]["pk"] == self.modelitem3.pk

        assert (
            response.data[0]["created"] < response.data[1]["created"] < response.data[2]["created"]
        )

        # By reverse sorting items from model1
        # We should get all 3 sorted from 3 to 1
        assert len(response.data) == 3
        response = self._test_get(
            "{}{}/?ordering=-created".format(ENDPOINTS["modelitem"], self.model1.name),
            self.admin_user,
            200,
        )
        assert response.data[0]["pk"] == self.modelitem3.pk
        assert response.data[1]["pk"] == self.modelitem2.pk
        assert response.data[2]["pk"] == self.modelitem1.pk

        assert (
            response.data[2]["created"] < response.data[1]["created"] < response.data[0]["created"]
        )

    def test_ordering_by_int_with_annotation(self):
        # By sorting from lowest price to highest
        # We should get the 2 items from model2 sorted
        # Here we are working with annotation. Price is in the json data field and is annotated
        # in the queryset so it can be sorted
        response = self._test_get(
            "{}{}/?ordering=price".format(ENDPOINTS["modelitem"], self.model2.name),
            self.admin_user,
            200,
        )
        assert len(response.data) == 2
        assert response.data[0]["pk"] == self.modelitem4.pk
        assert response.data[1]["pk"] == self.modelitem5.pk

        assert response.data[0]["data"]["price"] < response.data[1]["data"]["price"]

        # By reverse sorting price from items from model2
        # We should get prices sorted from highest to lowest
        assert len(response.data) == 2
        response = self._test_get(
            "{}{}/?ordering=-price".format(ENDPOINTS["modelitem"], self.model2.name),
            self.admin_user,
            200,
        )
        assert response.data[0]["pk"] == self.modelitem5.pk
        assert response.data[1]["pk"] == self.modelitem4.pk

        assert response.data[1]["data"]["price"] < response.data[0]["data"]["price"]

    def test_ordering_by_alphabetical_with_annotation(self):
        # By sorting by alphabetical order from A to Z
        # We should get the 3 items from model1 sorted
        # Here we are working with annotation. Price is in the json data field and is annotated
        # in the queryset so it can be sorted
        response = self._test_get(
            "{}{}/?ordering=text".format(ENDPOINTS["modelitem"], self.model1.name),
            self.admin_user,
            200,
        )
        assert len(response.data) == 3
        assert response.data[0]["pk"] == self.modelitem1.pk
        assert response.data[1]["pk"] == self.modelitem2.pk
        assert response.data[2]["pk"] == self.modelitem3.pk

        assert (
            response.data[0]["data"]["text"]
            < response.data[1]["data"]["text"]
            < response.data[2]["data"]["text"]
        )
        # By reverse sorting price from items from model2
        # We should get prices sorted from highest to lowest
        assert len(response.data) == 3
        response = self._test_get(
            "{}{}/?ordering=-text".format(ENDPOINTS["modelitem"], self.model1.name),
            self.admin_user,
            200,
        )
        assert response.data[0]["pk"] == self.modelitem3.pk
        assert response.data[1]["pk"] == self.modelitem2.pk
        assert response.data[2]["pk"] == self.modelitem1.pk

        assert (
            response.data[2]["data"]["text"]
            < response.data[1]["data"]["text"]
            < response.data[0]["data"]["text"]
        )


class ModelItemGroupsAPITestCase(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    """Modelitems groups assignment testcases."""

    @classmethod
    def setUpTestData(cls):
        settings = Settings.objects.first()
        cls.primary_lang = settings.primary_language
        cls.admin = AdminFactory()
        cls.user1 = UserFactory()
        cls.user2 = UserFactory()

        cls.group1 = GroupFactory(users=[cls.user1])
        cls.group2 = GroupFactory(users=[cls.user2])
        cls.group3 = GroupFactory(users=[cls.user1])

        cls.model = factories.CModelFactory(type="multiple")

    def test_create_modelitem_with_group_as_admin(self):
        data = {
            "lang": self.primary_lang.pk,
            "groups": [self.group1.pk],
            "data": {
                "title": "name of the object",
                "description": "description",
                "written": "1993-12-12",
                "price": 3.4,
                "content": "Hello how low",
                "picture": "https://candelapp.com/gif.gif",
            },
        }
        self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model.name),
            self.admin,
            201,
            data,
            as_json=True,
        )
        cmi = models.CModelItem.objects.last()
        assert cmi.groups.count() == 1

    def test_patch_modelitem_with_group_as_admin(self):
        cmi = factories.CModelItemFactory(model=self.model, groups=[self.group1])
        data = {
            "lang": self.primary_lang.pk,
            "groups": [self.group1.pk, self.group2.pk],
            "data": {
                "title": "name of the object",
                "description": "description",
            },
        }
        assert cmi.groups.count() == 1

        self._test_patch(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model.name, cmi.pk),
            self.admin,
            200,
            data,
            as_json=True,
            compare=False,
        )
        assert cmi.groups.count() == 2

    def test_create_modelitem_with_group_as_user(self):
        # With everyone can add permission
        self.model.can_add = {"allow_everyone": True}
        self.model.save()

        # It should not be possible to assign a group in which a user isn't
        data = {
            "lang": self.primary_lang.pk,
            "groups": [self.group2.pk],
            "data": {
                "title": "name of the object",
                "description": "description",
                "written": "1993-12-12",
                "price": 3.4,
                "content": "Hello how low",
                "picture": "https://candelapp.com/gif.gif",
            },
        }
        response = self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model.name),
            self.user1,
            400,
            data,
            as_json=True,
        )
        assert response.json() == {"groups": ["User groups do not match the provided groups."]}

        # With a group the user is in
        data["groups"] = [self.group1.pk]
        self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model.name),
            self.user1,
            201,
            data,
            as_json=True,
        )

        # Multiple groups
        data["groups"] = [self.group1.pk, self.group3.pk]
        response = self._test_post(
            "{}{}/".format(ENDPOINTS["modelitem"], self.model.name),
            self.user1,
            201,
            data,
            as_json=True,
        )
        assert len(response.json()["groups"]) == 2

    def test_update_modelitem_with_group_as_user(self):
        # With everyone can add permission
        self.model.can_edit = {"allow_everyone": True}
        self.model.save()

        # With a modelitem with group1 assigned
        cmi = factories.CModelItemFactory(model=self.model, groups=[self.group1])

        # It should not be possible to assign a group in which a user isn't
        data = {
            "lang": self.primary_lang.pk,
            "groups": [self.group2.pk],
            "data": {
                "title": "name of the object",
                "description": "description",
            },
        }
        response = self._test_patch(
            "{}{}/{}/".format(ENDPOINTS["modelitem"], self.model.name, cmi.pk),
            self.user2,
            400,
            data,
            as_json=True,
            compare=False,
        )
        assert response.json() == {"groups": ["A user can't update the ownership of an object."]}
