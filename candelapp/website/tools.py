"""Website related tools."""
from django.utils.text import slugify

from candelapp.settings.models import Settings
from candelapp.website.models import CPage, CModelItem


def build_modelitem_url(model, lang, title, primary_lang=None, page=None):
    """
    Build a url to reach the item from the templates.

    A complete url will be built if the object is linked to a page
    in its language. If there is no page, the url will be empty.

    Ex:
    - en modelitem, no page linked, secondary language:
    url: "/en/"
    - es modelitem, no page linked, primary language:
    url: "/"
    - fr modelitem, page linked in its language:
    /fr/page_name_in_fr/modelitem_name_in_fr/

    If the lang used is the primary language, the lang_code
    will not be added.

    Ex: primary language is "fr", secondary language is "en"
    fr: /page_name_in_fr/modelitem_name_in_fr/
    en: /en/page_name_in_en/modelitem_name_in_en/
    """
    if page is None:
        try:
            page = CPage.objects.get(model=model, lang=lang)
        except CPage.DoesNotExist:
            return ""

    if primary_lang is None:
        primary_lang = Settings.objects.first().primary_language

    url = "/{}/{}/".format(page.slug, slugify(title))

    # Add lang code to url as a prefix if this is not the primary language
    if primary_lang != lang:
        url = "/{}{}".format(lang.code, url)

    return url


def build_page_url(page, primary_lang):
    """Build a url to reach the page from the templates."""
    if page.is_index:
        # url should be /
        url = "/"
    else:
        # if the parent is the index, url should be /en/
        if page.parent is not None and page.parent.is_index is True:
            url = "/{}/".format(page.lang.code)
        else:
            # If primary lang, no lang code in url /page_name/
            if page.lang == primary_lang:
                url = "/{}/".format(page.slug)
            else:
                # url should be /en/page_name/
                url = "/{}/{}/".format(page.lang.code, page.slug)
    return url


def build_family_urls_with_lang_list(family_list, primary_lang=None):
    """
    Build a list of dict with lang code as key and sibling url as value.

    For modelitems, as url exists only when it is linked to a page, don't
    add any sibling without a url in the list.

    TODO: After page url saving overhaul, remove primary_lang from params and
    don't build url from here.
    """
    list_ = []
    if len(family_list) != 0:
        # Build page url from here
        # TODO: Save url when saving pages like modeitems
        if isinstance(family_list[0], CPage):
            for elem in family_list:
                list_.append({elem.lang.code: build_page_url(elem, primary_lang)})

        if isinstance(family_list[0], CModelItem):
            for elem in family_list:
                url = getattr(elem, "url")
                if url:
                    list_.append({elem.lang.code: url})

    return list_
