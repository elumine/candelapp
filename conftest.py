# Pytest configuration file.
# Makes pytest work with non-public schemas

from django.core.management import call_command
from django.db import connection

import pytest

from candelapp.settings.models import Settings
from candelapp.tenant.models import Domain, Tenant
from candelapp.tools import tools


schemas = [
    {"name": "public", "domain": "localhost"},
    {"name": "fsociety", "domain": "fsociety.localhost"},
    {"name": "sacredheart", "domain": "sacredheart.localhost"},
]


@pytest.fixture(scope="session")
def django_db_setup(django_db_setup, django_db_blocker):
    """Setup schemas for unit tests."""
    with django_db_blocker.unblock():
        call_command(
            "migrate_schemas",
            "--shared",
            interactive=False,
            verbosity=0,
        )

        fsociety_tenant = None

        # Create objects and related schemas
        for schema in schemas:
            with tools.suppress_stdout():
                tenant = Tenant(name=schema["name"], schema_name=schema["name"])
                tenant.save()
                domain = Domain(domain=schema["domain"], is_primary=True, tenant=tenant)
                domain.save()

                if schema["name"] == "fsociety":
                    fsociety_tenant = tenant

        # Set "fsociety" tenant as default one
        connection.set_tenant(fsociety_tenant)

        # Create settings for fsociety tenant
        Settings.objects.get_or_create()
