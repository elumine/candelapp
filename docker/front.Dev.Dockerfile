# develop stage
FROM node:15.8-alpine 

# Add perl for multiline replacing purpose
RUN apk add perl

ENV HOME=/home/node/

ARG UID=1000
ARG GID=1000
USER node

COPY ./front $HOME
WORKDIR $HOME

RUN yarn


