#!/bin/sh

# Postgresql db
./docker/wait-for-it.sh postgresql:5432 -s -t 0
echo "Postgresql server reachable."

pdm sync --dev

# Jupyter notebook setup
echo "c.NotebookApp.ip = '0.0.0.0'" > $HOME/.jupyter/jupyter_notebook_config.py
echo "c.NotebookApp.token = 'candelapp'" >> $HOME/.jupyter/jupyter_notebook_config.py
echo "c.NotebookApp.password = 'candelapp'" >> $HOME/.jupyter/jupyter_notebook_config.py
echo "c.NotebookApp.port = 8888" >> $HOME/.jupyter/jupyter_notebook_config.py
echo "c.NotebookApp.open_browser = False" >> $HOME/.jupyter/jupyter_notebook_config.py
echo "c.NotebookApp.iopub_data_rate_limit = 1e10" >> $HOME/.jupyter/jupyter_notebook_config.py
echo "c.NotebookApp.rate_limit_window = 10.0" >> $HOME/.jupyter/jupyter_notebook_config.py

echo "Jupyter notebook configuration set."

pdm run python3 manage.py migrate_schemas
pdm run python3 manage.py init_dev_tenants
pdm run gunicorn instance.wsgi:application --timeout 60 --workers 2 --reload --reload-engine inotify --bind 0.0.0.0:8000

tail -f /dev/null
