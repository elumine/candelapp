# Multilingual support

## How does it work

Candelapp's multilingual support operates on a main language and secondary languages model, similar to a parent-child relationship structure.

The 'main language' (parent) is the primary language setting for a particular object. Any additional languages needed for that object are considered 'secondary languages' (children). These secondary languages represent the international versions of the main language object.

The system functions in such a way that an object must first be created in the main language before it can be translated into any of the secondary languages. This ensures a structured and systematic approach to multilingual content management, where the main language version of an object serves as the reference point for all its translated versions.
