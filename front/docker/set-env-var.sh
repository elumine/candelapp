#!/bin/sh

# The whole purpose of this file is to add environment variables from docker
# to the client. Thus, it remains agnostic and requires only one build for all possible environments.

JSON_STRING='window.configs = {
    "BACKOFFICE_URL_PATTERN":"'"${BACKOFFICE_URL_PATTERN}"'",
    "API_URL_PATTERN":"'"${API_URL_PATTERN}"'"
};'

if [[ -f "dist/spa/index.html" ]]; then
    # Replacing in live envs
    JSON_STRING=$JSON_STRING perl -i -p0e 's/window\.configs = \{(.|\n)*\};/$ENV{JSON_STRING}/;' dist/spa/index.html
else
    # Multiline replacing dev env
    JSON_STRING=$JSON_STRING perl -i -p0e 's/window\.configs = \{(.|\n)*\};/$ENV{JSON_STRING}/;' src/index.template.html
fi

echo "Replacement done"
