import { LocalStorage, Notify } from "quasar";
import { store } from "src/store";

import axios from "axios";

import { getApiUrl } from "src/utils/api";

// Should be tested and raise notification when not setup correctly
const apiUrl = getApiUrl();

const apiInstance = axios.create({
  baseURL: apiUrl,
  headers: { "Content-Type": "application/json" },
});

apiInstance.interceptors.request.use(
  async function (config) {
    const token = await store.getters["auth/accessToken"];
    if (token) {
      config.headers["Authorization"] = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

apiInstance.interceptors.response.use(
  // Any status code that lies within the range of 2xx cause this function to trigger
  function (response) {
    return response;
  },
  // Any status codes that falls outside the range of 2xx cause this function to trigger
  async function (error) {
    // If the page gets refreshed, the token from the state is lost.
    // Try to get it again from LocalStorage
    if (error.response && error.response.status === 401) {
      if (error.config && !error.config._retry) {
        const token = LocalStorage.getItem("accessToken");
        if (token) {
          await store.dispatch("auth/setToken", token);
          error.config._retry = true;
          // Remove baseURL to avoid adding it to the current url
          // It makes something like "/api/v1/api/v1/..." and return obviously 404 response
          error.config.baseURL = "";
          return apiInstance(error.config);
        }
      } else {
        store.dispatch("auth/logoutUser");
        window.location.href = "#/login";
        Notify.create({
          message: "Session expirée. Veuillez vous reconnecter.",
          color: "info",
        });
      }
      // Error without response from the server or error with a 500+ response
    } else if (error.response && error.response.status == 500) {
      Notify.create({
        message:
          "Oups, petit problème technique. Merci de contacter le support.",
        color: "negative",
      });
    }
    // For any other errors
    return Promise.reject(error);
  }
);

export default apiInstance;
