import Vue from "vue";
import VueI18n from "vue-i18n";
import messages from "src/i18n";
import { Quasar } from "quasar";
import languages from "quasar/lang/index.json";

Vue.use(VueI18n);

const i18n = new VueI18n({
  // vue-i18n will find one of the available fallbacks automatically
  // Ex: if region 'fr-FR' not found, will look for global 'fr'
  // Else use fallbackLocale
  locale: navigator.language,
  fallbackLocale: "en-us",
  silentTranslationWarn: true,
  silentFallbackWarn: true,
  messages,
});

const getQuasarLang = (langIso) => {
  // May be "en-GB" or "fr"
  let langlowered = langIso.toLowerCase();

  // Consider fallback as regionless
  let regionLessLang;
  if (langlowered.split("-").length == 2) {
    regionLessLang = langlowered.split("-")[0];
  }
  let foundLangLowered = null;
  let foundRegionLessLang = null;
  let foundEnUs = null;

  // Try to find at least one of them else use ultimately en-us as fallback
  for (let lang of languages) {
    if (lang.isoName === langlowered) {
      foundLangLowered = lang;
      break;
    } else if (lang.isoName === regionLessLang) {
      foundRegionLessLang = lang;
    } else if (lang.isoName === "en-us") {
      foundEnUs = lang;
    }
  }
  return foundLangLowered || foundRegionLessLang || foundEnUs;
};

export default ({ app }) => {
  // Set i18n instance on app
  app.i18n = i18n;
  let langIso = Quasar.lang.getLocale();
  const quasarLang = getQuasarLang(langIso);
  try {
    import("quasar/lang/" + quasarLang.isoName).then((lang) => {
      Quasar.lang.set(lang.default);
    });
  } catch (err) {
    throw err;
  }
};

export { i18n };
