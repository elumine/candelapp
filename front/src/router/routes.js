import { i18n } from "src/boot/i18n";

const routes = [
  {
    path: "/",
    component: () => import("layouts/MenuLayout.vue"),
    meta: { requiresAuth: true, breadcrumb: "Admin" },
    children: [
      {
        path: "/pages",
        component: () => import("layouts/InternationalCRUDLayout.vue"),
        children: [
          {
            name: "pages-list",
            path: "",
            component: () => import("pages/website/Pages.vue"),
            meta: {
              breadcrumb: [{ name: i18n.tc("routes.pagesList") }],
            },
          },
          {
            name: "pages-create",
            path: "create",
            component: () => import("pages/website/PagesEdit.vue"),
            meta: {
              breadcrumb: [
                {
                  name: i18n.tc("routes.pagesList"),
                  link: "pages-list",
                },
                {
                  name: i18n.tc("routes.pagesCreate"),
                },
              ],
            },
          },
          {
            name: "pages-edit",
            path: "edit/:pk",
            component: () => import("pages/website/PagesEdit.vue"),
            meta: {
              breadcrumb: [
                {
                  name: i18n.tc("routes.pagesList"),
                  link: "pages-list",
                },
                {
                  name: i18n.tc("routes.pagesEdit"),
                },
              ],
            },
          },
          {
            name: "pages-international-edit",
            path: "edit/:parentPk/:langCode",
            component: () => import("pages/website/PagesEdit.vue"),
            meta: {
              breadcrumb: [
                {
                  name: i18n.tc("routes.pagesList"),
                  link: "pages-list",
                },
                {
                  name: i18n.tc("routes.pagesEdit"),
                },
              ],
            },
          },
        ],
      },
      {
        path: "/users",
        component: () => import("layouts/ContentLayout.vue"),
        children: [
          {
            name: "users-list",
            path: "",
            component: () => import("pages/website/Users.vue"),
            meta: { breadcrumb: [{ name: i18n.tc("routes.usersList") }] },
          },
          {
            name: "users-create",
            path: "create",
            component: () => import("pages/website/UsersEdit.vue"),
            meta: {
              breadcrumb: [
                {
                  name: i18n.tc("routes.usersList"),
                  link: "users-list",
                },
                {
                  name: i18n.tc("routes.usersCreate"),
                },
              ],
            },
          },
          {
            name: "users-edit",
            path: "edit/:pk",
            component: () => import("pages/website/UsersEdit.vue"),
            meta: {
              breadcrumb: [
                {
                  name: i18n.tc("routes.usersList"),
                  link: "users-list",
                },
                {
                  name: i18n.tc("routes.usersEdit"),
                },
              ],
            },
          },
        ],
      },
      {
        path: "/groups",
        component: () => import("layouts/ContentLayout.vue"),
        children: [
          {
            name: "groups-list",
            path: "",
            component: () => import("pages/website/Groups.vue"),
            meta: { breadcrumb: [{ name: i18n.tc("routes.groupsList") }] },
          },
          {
            name: "groups-create",
            path: "create",
            component: () => import("pages/website/GroupsEdit.vue"),
            meta: {
              breadcrumb: [
                {
                  name: i18n.tc("routes.groupsList"),
                  link: "groups-list",
                },
                {
                  name: i18n.tc("routes.groupsCreate"),
                },
              ],
            },
          },
          {
            name: "groups-edit",
            path: "edit/:pk",
            component: () => import("pages/website/GroupsEdit.vue"),
            meta: {
              breadcrumb: [
                {
                  name: i18n.tc("routes.groupsList"),
                  link: "groups-list",
                },
                {
                  name: i18n.tc("routes.groupsEdit"),
                },
              ],
            },
          },
        ],
      },
      {
        path: "/permissions",
        component: () => import("layouts/ContentLayout.vue"),
        children: [
          {
            name: "permissions-list",
            path: "",
            component: () => import("pages/website/Permissions.vue"),
            meta: { breadcrumb: [{ name: i18n.tc("routes.permissionsList") }] },
          },
          {
            name: "permissions-edit",
            path: "edit/:pk",
            component: () => import("pages/website/PermissionsEdit.vue"),
            meta: {
              breadcrumb: [
                {
                  name: i18n.tc("routes.permissionsList"),
                  link: "permissions-list",
                },
                {
                  name: i18n.tc("routes.permissionsEdit"),
                },
              ],
            },
          },
        ],
      },
      {
        path: "/models",
        component: () => import("layouts/ContentLayout.vue"),
        children: [
          {
            name: "models-list",
            path: "",
            component: () => import("pages/website/Models.vue"),
            meta: { breadcrumb: [{ name: i18n.tc("routes.modelsList") }] },
          },
          {
            name: "models-create",
            path: "create",
            component: () => import("pages/website/ModelsEdit.vue"),
            meta: {
              breadcrumb: [
                {
                  name: i18n.tc("routes.modelsList"),
                  link: "models-list",
                },
                {
                  name: i18n.tc("routes.modelsCreate"),
                },
              ],
            },
          },
          {
            name: "models-edit",
            path: "edit/:pk",
            component: () => import("pages/website/ModelsEdit.vue"),
            meta: {
              breadcrumb: [
                {
                  name: i18n.tc("routes.modelsList"),
                  link: "models-list",
                },
                {
                  name: i18n.tc("routes.modelsEdit"),
                  link: "models-edit",
                },
              ],
            },
          },
        ],
      },
      {
        path: "/modelitems",
        component: () => import("layouts/InternationalCRUDLayout.vue"),
        children: [
          {
            name: "modelitems-list",
            path: ":model_name",
            component: () => import("pages/website/ModelItems.vue"),
            meta: { breadcrumb: [{ name: i18n.tc("routes.modelItemsList") }] },
          },
          {
            name: "modelitems-create",
            path: ":model_name/create",
            component: () => import("pages/website/ModelItemsEdit.vue"),
            meta: {
              breadcrumb: [
                { name: i18n.tc("routes.modelItemsList") },
                {
                  name: i18n.tc("routes.modelItemsCreate"),
                },
              ],
            },
          },
          {
            name: "modelitems-edit",
            path: ":model_name/edit/:pk",
            component: () => import("pages/website/ModelItemsEdit.vue"),
            meta: {
              breadcrumb: [
                { name: i18n.tc("routes.modelItemsList") },
                {
                  name: i18n.tc("routes.modelItemsEdit"),
                },
              ],
            },
          },
          {
            name: "modelitems-international-edit",
            path: ":model_name/edit/:parentPk/:langCode",
            component: () => import("pages/website/ModelItemsEdit.vue"),
            meta: {
              breadcrumb: [
                { name: i18n.tc("routes.modelItemsList") },
                {
                  name: i18n.tc("routes.modelItemsEdit"),
                },
              ],
            },
          },
        ],
      },
      {
        name: "theme-edit",
        path: "theme",
        component: () => import("pages/website/Theme.vue"),
        meta: {
          breadcrumb: [
            {
              name: i18n.tc("routes.importTheme"),
            },
          ],
        },
      },
      {
        path: "",
        component: () => import("layouts/InternationalCRUDLayout.vue"),
        children: [
          {
            name: "settings-edit",
            path: "settings",
            component: () => import("pages/website/Settings.vue"),
            meta: {
              breadcrumb: [
                {
                  name: i18n.tc("routes.settings"),
                },
              ],
            },
          },
          {
            name: "logs-list",
            path: "logs",
            component: () => import("pages/website/Logs.vue"),
            meta: {
              breadcrumb: [
                {
                  name: i18n.tc("routes.logs"),
                },
              ],
            },
          },
        ],
      },
    ],
  },
  {
    path: "/login",
    component: () => import("layouts/LoginLayout.vue"),
    children: [
      { name: "login", path: "", component: () => import("pages/Login.vue") },
    ],
  },
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue"),
  });
}

export default routes;
