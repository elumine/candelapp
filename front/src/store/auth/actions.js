import { LocalStorage } from "quasar";

import apiInstance from "src/api/base";

export function initAuth(state) {
  let token = LocalStorage.getItem("accessToken");
  let user = LocalStorage.getItem("user");
  if (token !== "" || token !== undefined) {
    state.commit("SET_ACCESS_TOKEN", token);
  }
  if (user !== "" || user !== undefined) {
    state.commit("SET_USER", JSON.parse(user));
  }
}

export async function getUser(state) {
  try {
    let response = await apiInstance.get("/auth/me/");
    state.commit("SET_USER", response.data);
    LocalStorage.set("user", JSON.stringify(response.data));
  } catch (error) {
    throw error;
  }
}

export function setToken(state, data) {
  try {
    state.commit("SET_ACCESS_TOKEN", data);
  } catch (error) {
    throw error;
  }
}

export async function loginUser(state, data) {
  try {
    let response = await apiInstance.post("/auth/login/", data);
    setToken(state, response.data.token);
    LocalStorage.set("accessToken", response.data.token);
    // Add fallback with cookies if LocalStorage doesn't work
    // Encapsulate LocalStorage in a try catch for bulletproof
    await getUser(state);
  } catch (error) {
    throw error;
  }
}

export async function logoutUser(state, data) {
  state.commit("DESTROY_ACCESS_TOKEN");
  state.commit("DESTROY_USER");
  LocalStorage.clear();
}
