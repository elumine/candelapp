export function SET_USER(state, user) {
  state.user = user;
}

export function DESTROY_USER(state, user) {
  state.user = {};
}

export async function SET_ACCESS_TOKEN(state, accessToken) {
  state.accessToken = accessToken;
}

export function DESTROY_ACCESS_TOKEN(state) {
  state.accessToken = "";
}
