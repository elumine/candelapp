import _ from "lodash";

export async function SET_DYNAMIC_MENU(state, data) {
  data.sort((a, b) => a.category_name.localeCompare(b.category_name));
  state.menu = _.groupBy(data, "category_name");
}
