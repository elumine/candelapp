import Vue from "vue";

import Vuex from "vuex";

import auth from "./auth";
import dynamicmenu from "./dynamicmenu";
import settings from "./settings";

Vue.use(Vuex);

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

let store = null;
export default function(/* { ssrContext } */) {
  store = new Vuex.Store({
    modules: {
      auth,
      dynamicmenu,
      settings,
    }
  });

  return store;
}

// Make the store available app wide
export { store };
