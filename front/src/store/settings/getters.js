export function getSettings(state) {
  return state.settings;
}

export function getLanguages(state) {
  return state.languages;
}
