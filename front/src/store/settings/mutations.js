import { LocalStorage } from "quasar";

export async function SET_SETTINGS(state, data) {
  state.settings = {
    ...state.settings,
    ...data,
  };
  LocalStorage.set("settings", JSON.stringify(state.settings));
}

export async function SET_INTERFACE_SETTINGS(state, data) {
  state.settings = {
    ...state.settings,
    ["colors"]: data.colors,
    ["logo"]: data.logo,
  };
  LocalStorage.set("settings", JSON.stringify(state.settings));
}

export async function SET_LANGUAGES(state, data) {
  state.languages = {
    ...state.languages,
    ...data,
  };
  LocalStorage.set("languages", JSON.stringify(state.languages));
}
