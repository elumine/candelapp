"""candelapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf import settings
from django.urls import include, path

from drf_spectacular.views import (
    SpectacularAPIView,
    SpectacularSwaggerView,
)


urlpatterns = [
    path("api/v1/auth/", include("candelapp.authentication.urls")),
    path("api/v1/groups/", include("candelapp.groups.urls")),
    path("api/v1/history/", include("candelapp.history.urls")),
    path("api/v1/international/", include("candelapp.international.urls")),
    path("api/v1/permissions/", include("candelapp.permissions.urls")),
    path("api/v1/settings/", include("candelapp.settings.urls")),
    path("api/v1/website/", include("candelapp.website.urls")),
    path("api/v1/", SpectacularAPIView.as_view(), name="schema"),
    path(
        "api/v1/schema/",
        SpectacularSwaggerView.as_view(url_name="schema"),
        name="swagger-ui",
    ),
]

if settings.DEBUG:
    import debug_toolbar
    from django.conf.urls.static import static

    urlpatterns += [
        path("__debug__/", include(debug_toolbar.urls)),
        path("api/v1/api-auth/", include("rest_framework.urls")),
    ]
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
